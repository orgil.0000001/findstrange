﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

[RequireComponent(typeof(Rigidbody))]
public class BotRandomMoveController : Mb {
    public float vel = 10, rotSpd = 5, minTime = 1.5f, maxTime = 3;
    
    float curAng = 0, ang = 0, t, dt = 0;

    void Start() {
        rb.NoG();
        rb.Con(false, true, false, true, true, true);
    }

    void Update() {
        if (IsPlaying) {
            dt += Dt;
            if (dt > t) {
                dt = 0;
                t = Rnd.Rng(minTime, maxTime);
                ang = Rnd.Ang;
            }
            curAng = M.Lerp(curAng, ang, Dt * rotSpd);
            Tr = Q.Y(curAng);
            rb.V(F * vel);
        } else
            rb.V0();
    }
}
