namespace Orgil {
	public enum Tag { Untagged, Respawn, Finish, EditorOnly, MainCamera, Player, GameController, Diamond, Bot, Coin, Enemy, Wall }
	public class Lyr { public const int Default = 0, TransparentFX = 1, IgnoreRaycast = 2, Player = 3, Water = 4, UI = 5, Enemy = 6, Wall = 7, Table = 8; }
	public class Lm { public const int Default = 1, TransparentFX = 2, IgnoreRaycast = 4, Player = 8, Water = 16, UI = 32, Enemy = 64, Wall = 128, Table = 256; }
}