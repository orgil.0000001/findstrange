using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using Polygon;

public class PolyKnights : Mb {
    public enum HouseRoom { Foundation_07, Foundation_Beams_01, FoundationRock_01, Room_07, RoomRound_01, RoomRound_02, RoomTall_06, RoomTop_07, TopRoomSmall_07, Tower_01 }
    public enum HouseExt { Chimney_01, Chimney_02, Chimney_03, Chimney_04, Chimney_05, Door_01, Door_02, Door_03, Door_04, Extension_01, Extension_02, StepsSmall_01, Window_01, Window_02, Window_03, Window_04, Window_05 }
    public enum Other { Leanto_01, Tent_01, Tent_02, Tent_03, Village_Well_01 }
    public enum Ct { Block, Lite, Snow }
    public enum Path { Cobble, Stone, Tile }
    public enum Mat { _01_Dark, _01, _Variation_02_Dark, _Variation_02, _Variation_03_Dark, _Variation_03, _Variation_04_Dark, _Variation_04, _Variation_Snow_To_Grass_Swap }
    public enum MatChr { Black, Blue, Orange, Yellow }
    public enum Nature {
        RockPile_01, RockPile_02, RockPile_03, Tree_01, Tree_02, Tree_03, Tree_Twisted_01, Tree_Twisted_02,
        Cliff_01, Cliff_02, Cloud_06, Flower_01, Grass_01, GroundMound_01, GroundMound_02, MountainGrass_01, MountainGrass_02, MountainSnow_01, MountainSnow_02
    }
    static float CastleTowerDis = 7, CastleWallDis = 4.1f, CastleTowerH = 3, CastleWallH = 5, ChurchTowerH = 3.25f, StairsLargeSz = 2.88f, PathSz = 3;
    public static Dictionary<string, string> HouseAbrDic = new Dictionary<string, string>() {
        { "d", "(0,0,1.5,0);Door_03 " }, { "f", "(0,0,0,90);Foundation_07;oooooo " },
        { "w1_0", "(1.5,5,4,90);Window_02 " }, { "w2_0", "(-1.5,5,4,-90);Window_02 " },
        { "d_0", "(0,0,1.3,0);Door_02 " }, { "f_0", "(0,0,3,0);Foundation_Beams_01;oooooo " }, { "rtp_0", "(0,2.7,3,0);RoomTop_07;oooooo " },
        { "w1", "(1.5,5,1.5,0);Window_02 " }, { "w2", "(-1.5,5,1.5,0);Window_02 " }, { "w3", "(1.5,5,-1.5,180);Window_02 " }, { "w4", "(-1.5,5,-1.5,180);Window_02 " },
        { "rtp", "(0,2.7,0,90);RoomTop_07;oooooo " },{ "rts", "(0,4,-1.5,90);TopRoomSmall_07;oooooo " }, { "rts2", "(0,5,-1.5,90);TopRoomSmall_07;oooooo " }, { "rtl", "(-1.5,6,0,0);RoomTall_06;oooo " },
        { "w1_2", "(1.5,9,0,0);Window_02 " }, { "w2_2", "(-1.5,9,0,0);Window_02 " }, { "w3_2", "(1.5,9,-3,180);Window_02 " }, { "w4_2", "(-1.5,9,-3,180);Window_02 " },
        { "rtp_2", "(0,6.7,-1.5,90);RoomTop_07;oooooo " },{ "rts_2", "(0,8,0,90);TopRoomSmall_07;oooooo " }, { "rts2_2", "(0,9,0,90);TopRoomSmall_07;oooooo " }, { "rtl_2", "(-1.5,10,-1.5,0);RoomTall_06;oooooo " },
        { "w1_3", "(1.5,13,1.5,0);Window_02 " }, { "w2_3", "(-1.5,13,1.5,0);Window_02 " }, { "w3_3", "(1.5,13,-1.5,180);Window_02 " }, { "w4_3", "(-1.5,13,-1.5,180);Window_02 " },
        { "rtp_3", "(0,10.7,0,90);RoomTop_07;oooooo " },{ "rts_3", "(0,12,-1.5,90);TopRoomSmall_07;oooooo " }, { "rts2_3", "(0,13,-1.5,90);TopRoomSmall_07;oooooo " }, { "rtl_3", "(-1.5,14,0,0);RoomTall_06;oooo " },
    };
    public static List<List<Vector4>> houseDatas = List(
        List(V4.W(90), V4.Z(1.5f), V4.Z(1.3f), V4.Z(3), V4.Yz(2.7f, 3), V4.V(1.5f, 5, 4, 90), V4.V(-1.5f, 5, 4, -90)),
        List(V4.W(90), V4.Yzw(1.3f, -1.5f, 90), V4.Yzw(2.3f, -1.5f, 90), V4.Xy(-1.5f, 3.3f), V4.Xyz(1.5f, 2.3f, 1.5f), V4.Xyz(-1.5f, 2.3f, 1.5f))
    );
    public static void CrtCanalBridge(Transform parTf, Vector3 p, float ry, float len, bool isSnow = false, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Canal_Bridge", pTf, parTf);
        Material mat = GetMat(mt);
        for (int i = 0, n = M.RoundI(len / PathSz); i < n; i++)
            Poly.CrtPfGo(EnvPf("Canal_Bridge_01" + (isSnow ? "_Snow" : ""), isSnow), pTf * Tf.P(V3.Z(0.5f + i) * PathSz), par2Tf, mat);
    }
    public static void CrtCanal(Transform parTf, Vector3 p, float ry, string data, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Canal", pTf, parTf);
        Material mat = GetMat(mt);
        Vector3 dp = V3.O;
        bool isFill = Poly.IsFill(data);
        string[] datas = data.RgxSplitSpc();
        int n = datas.Length - 1, corPrv = isFill ? Poly.DirCor(datas[n][0], datas[0][0]) : 0, corLast = corPrv;
        for (int i = 0; i <= n; i++) {
            int d = Poly.DirI(datas[i][0]), cor2 = i == n ? corLast : Poly.DirCor(d, Poly.DirI(datas[i + 1][0])), m = datas[i].Sub(1).I() - 1;
            for (int j = 0; j <= m; j++) {
                dp += Poly.DirV3(d) * PathSz / 2;
                int cor = j == m ? cor2 : 0;
                if (!((corPrv == -1 || corPrv == 1) && j == 0))
                    Poly.CrtPfGo(EnvPf("Canal_0" + (cor == 0 ? 1 : cor == 1 ? 2 : 3)), pTf * Tf.PR(dp, (d + cor - 2) * 90) * Tf.P(V3.Xz(cor + 1, cor) * PathSz / 2), par2Tf, mat);
                dp += Poly.DirV3(d) * PathSz / 2;
            }
            corPrv = cor2;
        }
    }
    public static void CrtPathTile(Transform parTf, Vector3 p, float ry, string data, bool isSnow = false, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry), lpTf = Tf.P(V3.pzp * PathSz / 2);
        Transform par2Tf = Poly.NewGoTf("Path_Tile", pTf, parTf);
        Material mat = GetMat(mt);
        Vector3 dp = V3.O;
        bool isFill = Poly.IsFill(data);
        string[] datas = data.RgxSplitSpc();
        for (int i = 0, n = datas.Length - 1; i <= n; i++)
            for (int j = 0, d = Poly.DirI(datas[i][0]), m = datas[i].Sub(1).I() - 1; j <= m; j++) {
                if (!(i == 0 && j == 0))
                    dp += Poly.DirV3(d) * PathSz;
                int cor = j == m ? i == n ? isFill ? Poly.DirCor(d, Poly.DirI(datas[0][0])) : 0 : Poly.DirCor(d, Poly.DirI(datas[i + 1][0])) : 0;
                Poly.CrtPfGo(EnvPf("Path_Tile_" + (cor != 0 ? "Corner_" : "") + "0" + (cor != 0 ? 1 : Rnd.RngIn(1, 2)) + (isSnow ? "_Snow" : ""), isSnow), pTf * Tf.PR(dp, (cor <= 0 ? d + 1 : d) * 90) * lpTf, par2Tf, mat);
            }
    }
    public static void CrtPath(Transform parTf, Vector3 p, float ry, float lenX, float lenZ, bool isCobble = true, bool isSnow = false, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry), lpTf = Tf.P(V3.pzp * PathSz / 2);
        Transform par2Tf = Poly.NewGoTf("Path", pTf, parTf);
        Material mat = GetMat(mt);
        for (int i = 0, x = M.RoundI(lenX / PathSz), z = M.RoundI(lenZ / PathSz); i < z; i++)
            for (int j = 0; j < x; j++)
                Poly.CrtPfGo(EnvPf("Path_" + (isCobble ? "Cobble" : "Stone") + "_0" + Rnd.RngIn(1, isCobble ? 2 : 3) + (isSnow ? "_Snow" : ""), isSnow), pTf * Tf.PR(V3.Xz(j * PathSz, i * PathSz), isCobble ? Rnd.B ? 0 : 180 : Rnd.Rng(0, 4) * 90) * lpTf, par2Tf, mat);
    }
    public static void CrtNature(Transform parTf, Tf tf, Nature nature, bool isSnow = false, Mat mt = Mat._01) {
        Poly.CrtPfGo(EnvPf(nature + ((int)nature < 8 && isSnow ? "_Snow" : ""), (int)nature < 8 && isSnow), tf, parTf, GetMat(mt));
    }
    public static void CrtStairsLarge(Transform parTf, Vector3 p, float ry, string data, bool isSnow = false, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry), lpTf = Tf.P(V3.pzn * StairsLargeSz / 2);
        Transform par2Tf = Poly.NewGoTf("Stairs_Large", pTf, parTf);
        Material mat = GetMat(mt);
        Vector3 dp = V3.O;
        bool isFill = Poly.IsFill(data);
        string[] datas = data.RgxSplitSpc();
        for (int i = 0, n = datas.Length - 1; i <= n; i++)
            for (int j = 0, d = Poly.DirI(datas[i][0]), stair = Poly.DirI(datas[i][1]) < 0 ? 1 : 2, m = datas[i].Sub(stair).I() - 1; j <= m; j++) {
                if (!(i == 0 && j == 0))
                    dp += Poly.DirV3(d) * StairsLargeSz;
                int cor = j == m ? i == n ? isFill ? Poly.DirCor(d, Poly.DirI(datas[0][0])) : 0 : Poly.DirCor(d, Poly.DirI(datas[i + 1][0])) : 0;
                Poly.CrtPfGo(BldPf("StairsLarge_" + (cor != 0 ? "Corner_" : "") + "0" + stair + (isSnow ? "_Snow" : ""), isSnow), pTf * Tf.PR(dp, (cor <= 0 ? d : d - 1) * 90) * lpTf, par2Tf, mat);
            }
    }
    public static void CrtHouse(Transform parTf, Vector3 p, float ry, int floorCnt, bool isBeam = false, bool isWindow = false, bool isRoomTall = false, bool isTopRoomSmall = false, bool isSnow = false, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("House", pTf, parTf), floorTf = Poly.NewGoTf("Floor_1", pTf, par2Tf);
        for (int i = 0; i < 7; i++)
            if (isBeam ? i != 1 : i < 2) {
                Tf p2Tf = pTf * Tf.PR(houseDatas[0][i], houseDatas[0][i].w);
                if (i == 0 || i == 3 || (i == 4 && floorCnt != 1))
                    CrtHouseRoom(floorTf, p2Tf.p, p2Tf.r.y, i == 0 ? (floorCnt == 1 ? HouseRoom.Room_07 : HouseRoom.Foundation_07) : i == 3 ? HouseRoom.Foundation_Beams_01 : HouseRoom.RoomTop_07, "oooooo", true, isSnow, mt);
                else if (i == 1 || i == 2 || ((i == 5 || i == 6) && floorCnt != 1))
                    CrtHouseExt(floorTf, p2Tf, i == 1 ? HouseExt.Door_03 : i == 2 ? HouseExt.Door_02 : HouseExt.Window_02, isSnow, mt);
            }
        for (int i = floorCnt == 1 ? 0 : 1, n = floorCnt - 1; i < floorCnt; i++) {
            Tf p2Tf = Tf.PR(V3.Yz(floorCnt == 1 ? 0.4f : 2.7f + (i - 1) * 4, floorCnt > 1 && i.IsEven() ? -1.5f : 0), floorCnt == 1 || i.IsOdd() ? 0 : 180);
            if (floorCnt > 1)
                floorTf = Poly.NewGoTf("Floor_" + (i + 1), p2Tf, par2Tf);
            if (i == n) {
                CrtHouseExt(floorTf, p2Tf * Tf.P((isRoomTall ? V3.Y(4) : isTopRoomSmall ? V3.V(2, 3, -1.5f) : V3.Xy(2, 1.2f)) + V3.Y(i <= 1 ? -0.4f : 0)), HouseExt.Chimney_03, false, Mat._01);
            }
            for (int j = 0; j < 6; j++) {
                Tf p3Tf = pTf * p2Tf * Tf.PR(houseDatas[1][j], houseDatas[1][j].w);
                if (j < 4) {
                    if (j == 3 ? (i == n && isRoomTall) : j == 1 ? (i < n) : j == 2 ? (i == n && isTopRoomSmall) : floorCnt > 1)
                        CrtHouseRoom(floorTf, p3Tf.p, p3Tf.r.y, j == 0 ? HouseRoom.RoomTop_07 : j == 1 || j == 2 ? HouseRoom.TopRoomSmall_07 : HouseRoom.RoomTall_06, "oooooo", true, isSnow, mt);
                } else if (!(i == 1 && isBeam) && isWindow)
                    CrtHouseExt(floorTf, p3Tf, HouseExt.Window_02, isSnow, mt);
            }
        }
    }
    public static void CrtHouse(Transform parTf, Vector3 p, float ry, string data, bool isSnow = false, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("House", pTf, parTf);
        string[] datas = data.RgxSplitSpc();
        for (int i = 0; i < datas.Length; i++) {
            if (HouseAbrDic.ContainsKey(datas[i].L()))
                datas[i] = HouseAbrDic[datas[i]];
            string[] arr = datas[i].Split(';');
            Vector4 pr = arr[0].V4();
            if (arr.Length == 3) {
                CrtHouseRoom(par2Tf, pr.V3(), ry + pr.w, arr[1].Enum<HouseRoom>(), arr[2], true, isSnow, mt);
            } else if (arr.Length == 2) {
                CrtHouseExt(par2Tf, Tf.PR(pr.V3(), ry + pr.w), arr[1].Enum<HouseExt>(), isSnow, mt);
            }
        }
    }
    public static void CrtHouseRoom(Transform parTf, Vector3 p, float ry, HouseRoom room, string window, bool isWindow4 = true, bool isSnow = false, Mat mt = Mat._01) {
        int r = (int)room, n = Arr(6, 0, 6, 6, 2, 2, 4, 6, 4, 2)[r];
        Vector4 a = Arr(V4.V(1.33f, 1.05f, 0.8f, 2.23f), V4.O, V4.V(1.33f, 1.05f, 0.8f, 2.23f), V4.V(1.53f, 1.05f, 0.8f, 2.43f), V4.Xy(1.68f, 1.2f), V4.Xy(1.43f, 1.1f), V4.Xyz(1.23f, 2.1f, 1.23f), V4.V(1.83f, 0.4f, 1, 2.73f), V4.Xyz(1.33f, 0.85f, 1.75f), V4.Xy(0.68f, 2.5f))[r];
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf(room.tS().SubLast(0, 3), pTf, parTf), windowTf = Poly.NewGoTf("Window", pTf, par2Tf);
        Poly.CrtPfGo(BldPf("House_" + room + (isSnow && r >= 3 ? "_Snow" : ""), isSnow && r >= 3), Tf.PR(p, ry + (r == 5 ? 90 : 0)), par2Tf, GetMat(mt));
        for (int i = 0; i < window.Length && i < n; i++)
            if (window[i] != '_')
                CrtHouseExt(windowTf, pTf * Tf.PR(n == 2 ? V3.Xy(i == 0 ? a.x : -a.x, a.y) : n == 4 ? V3.V(i == 0 ? a.x : i == 2 ? -a.x : 0, a.y, i == 1 ? a.z : i == 3 ? -a.z : 0) : V3.V(i <= 1 ? a.x : i == 3 || i == 4 ? -a.x : 0, a.y, i == 1 || i == 3 ? a.z : i.IsMod(4) ? -a.z : i == 2 ? a.w : -a.w), V3.Xy(r == 9 ? 5 : r == 8 && i.IsEven() ? 15 : 0, n == 2 ? (i == 0 ? 90 : -90) : n == 4 ? (i == 0 ? 90 : i == 1 ? 0 : i == 2 ? -90 : 180) : (i <= 1 ? 90 : i == 2 ? 0 : i <= 4 ? -90 : 180))), isWindow4 ? HouseExt.Window_04 : HouseExt.Window_05, isSnow, mt);
    }
    public static void CrtHouseExt(Transform parTf, Tf tf, HouseExt ext, bool isSnow = false, Mat mt = Mat._01) {
        Poly.CrtPfGo(BldPf("House_" + ext + (isSnow ? "_Snow" : ""), isSnow), tf * Tf.P(Arr(V3.O, V3.O, V3.O, V3.Z(-0.83f), V3.Xz(-0.425f, 0.373f), V3.Z(-0.043f), V3.O, V3.Z(0.95f), V3.Z(0.78f), V3.Z(0.95f), V3.Z(1.685f), V3.O, V3.Z(0.74f), V3.O, V3.O, V3.Z(0.19f), V3.Z(0.18f))[(int)ext]), parTf, GetMat(mt));
    }
    public static void CrtOther(Transform parTf, Vector3 p, float ry, Other other, bool isSnow = false, Mat mt = Mat._01) {
        Poly.CrtPfGo(BldPf(other + (isSnow ? "_Snow" : ""), isSnow), Tf.PR(p, ry), parTf, GetMat(mt));
    }
    // a = Archway, s = Stairs, S = StairArchway, Straight
    // <Data><RotationY>
    public static void CrtRockwall(Transform parTf, Vector3 p, float ry, string data, bool isSnow = false, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Rockwall", pTf, parTf);
        string[] datas = data.RgxSplitSpc();
        Vector3 dp = V3.O;
        int stair = 0, ps = 0;
        Material mat = GetMat(mt);
        for (int i = 0; i < datas.Length; i++) {
            char c = datas[i][0];
            bool isS = c.L() == 's', isSw = c == 's', isSa = c == 'S', isA = c.L() == 'a', isW = !isS && !isA, isSs = (stair + isS.I()).IsOdd(), isSta = i > 0 && stair == 0 && isS;
            if (isS)
                stair++;
            float ang = datas[i].Sub(1).F(), d = isSs ? (ps == 1 ? (isA ? -0.85f : -0.7f) : ps == 2 ? (isA ? -0.95f : -0.8f) : 0) : 0;
            Poly.CrtPfGo(BldPf("Rockwall_" + (isA ? "Archway" : isSw ? "Stairs" : isSa ? "StairArchway" : "Straight") + "_01" + (isSnow ? "_Snow" : ""), isSnow), pTf * Tf.PR(dp, ang) * Tf.PR(V3.Z((isA ? 1.155f : isSw ? 1.88f : isSa ? 2.18f : 1.73f) + (isSta ? 1.5f : 0) + d), isS && isSs ? 180 : 0), par2Tf, mat);
            dp += Ang.Xz(Ang.Unity(ang), (isA ? 1.8f : isSw ? 4.1f : isSa ? 4.8f : 3.1f) + d + (isSta || !(stair == 0 || isSs) ? 1.5f : 0));
            ps = isSw ? 1 : isSa ? 2 : 0;
        }
    }
    public static void CrtChurch(Transform parTf, Vector3 p, float ry, string tower, string window, bool isExt, Ct type = Ct.Block, Mat mt = Mat._01) {
        string liteStr = type == Ct.Lite ? "_Lite" : "", snowStr = type == Ct.Block ? "" : "_" + type;
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Church", pTf, parTf);
        Material mat = GetMat(mt);
        Poly.CrtPfGo(BldPf("Church_Room_03" + snowStr, type == Ct.Snow), pTf, par2Tf, mat);
        if (isExt)
            Poly.CrtPfGo(BldPf("Church_Extension_01" + snowStr, type == Ct.Snow), pTf * Tf.P(V3.Z(5)), par2Tf, mat);
        Poly.CrtPfGo(BldPf("Church_Door_01" + (type == Ct.Snow ? "_Snow" : ""), type == Ct.Snow), pTf * Tf.P(V3.Z(isExt ? 5.2f : 2.6f)), par2Tf, mat);
        CrtChurchTower(par2Tf, Tf.Pnt(pTf, V3.Z(-5)), ry, tower, type, mt);
        Transform windowTf = Poly.NewGoTf("Church_Window", pTf, par2Tf);
        for (int i = 0; i < window.Length && i < 6; i++)
            if (window[i] != '_')
                Poly.CrtPfGo(BldPf("Church_Window_01"), pTf * Tf.PR(V3.V((i < 3).Sign() * 2.1f, 1.35f, (i % 3 - 1) * (i < 3).Sign() * 1.75f), (i < 3).Sign() * 90), windowTf, mat);
    }
    // b = TowerBase, f = Tower_03, R = Tower_01, r = Tower_02
    public static void CrtChurchTower(Transform parTf, Vector3 p, float ry, string tower, Ct type = Ct.Block, Mat mt = Mat._01) {
        if (tower != "") {
            string liteStr = type == Ct.Lite ? "_Lite" : "", snowStr = type == Ct.Block ? "" : "_" + type;
            Tf pTf = Tf.PR(p, ry);
            Transform par2Tf = Poly.NewGoTf("Church_Tower", pTf, parTf);
            Material mat = GetMat(mt);
            for (int i = 0; i < tower.Length; i++)
                Poly.CrtPfGo(BldPf("Church_Tower" + (tower[i].L() == 'r' ? "_0" + (tower[i] == 'r' ? 2 : 1) + snowStr : (tower[i].L() == 'b' ? "Base_01" : "_03") + liteStr), tower[i].L() == 'r' && type == Ct.Snow), pTf * Tf.P(V3.Y(i * ChurchTowerH)), par2Tf, mat);
        }
    }
    public static string CastleRoundStr(int n, int d, int m, float r, string w, string t1, string t2 = "", bool isIronGate = true) {
        List<List<string>> lis = new List<List<string>>() { new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>() };
        for (int i = 0; i < 2; i++) {
            for (int j = 0, k = i * 4; j < n; lis[k].Add(w), j++)
                lis[k].Add((j == 0 ? t1 : t2) + (j > 0 && t2 == "" ? w : "" + (i == 0 ? 90 : 270)));
            for (int j = 0, k = i * 4 + 2; j < m; lis[k].Add(w), j++)
                lis[k].Add((j == 0 ? t1 : t2) + (j > 0 && t2 == "" ? w : "" + (i == 0 ? 180 : 0)));
        }
        for (int i = 1; i <= 7; i += 2)
            for (int j = 0; j < d; lis[i].Add(w), j++)
                lis[i].Add((j == 0 ? t1 : t2) + (j > 0 && t2 == "" ? w : "" + (i == 1 ? 180 - r : i == 3 ? 180 + r : i == 5 ? 360 - r : r)));
        lis[0][n] = (isIronGate ? "i" : "d") + w.Sub(1);
        for (int i = 0; i < n; i++) {
            lis[7].Add(lis[0][0]);
            lis[0].RemoveAt(0);
        }
        string s = "";
        for (int i = 0; i < lis.Count; i++)
            for (int j = 0; j < lis[i].Count; j++)
                s += lis[i][j] + " ";
        return s.Trim();
    }
    public static void CrtCastle(Transform parTf, Vector3 p, float ry, string data, Ct type = Ct.Block, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Castle", pTf, parTf);
        string[] datas = data.RgxSplitSpc();
        bool isStaWall = !datas[0].Contains(";");
        float rotY = isStaWall ? 90 : 0;
        Vector3 dir = Ang.Xz(Ang.Unity(rotY)), dp = isStaWall ? dir * CastleTowerDis / 2 : V3.O;
        for (int i = 0, j = isStaWall ? 0 : 1; i < datas.Length; i++) {
            string[] arr = datas[i].Split(';');
            if (arr[0].L().IsS("t")) {
                dp = dp + dir * (CastleWallDis * (j - 1));
                CrtCastleTower(par2Tf, Tf.Pnt(pTf, dp), ry + rotY, arr[1], type, mt);
                rotY = arr[2].F();
                dir = Ang.Xz(Ang.Unity(rotY));
                dp += dir * CastleTowerDis;
                j = 0;
            } else {
                CrtCastleWall(par2Tf, Tf.Pnt(pTf, dp + dir * (CastleWallDis * j - CastleTowerDis / 2)), ry + rotY - 90, datas[i], type, mt);
                j++;
            }
        }
    }
    // o = Round, b = Base, f = Floor, v = Wood_Battlement_01, V = Roof_Spire_01, Base2
    // t;[Round],[Floor];[ArrowSlit]
    public static void CrtCastleTower(Transform parTf, Vector3 p, float ry, string data, Ct type = Ct.Block, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("CastleTower", pTf, parTf);
        string[] arr = data.Split(',');
        bool isRound = arr[0].L() == "o";
        string n = arr[1], arrowSlit = arr.Length > 2 ? arr[2] : "1111", roundStr = isRound ? "Round_" : "", liteStr = type == Ct.Lite ? "_Lite" : "", snowStr = type == Ct.Block ? "" : "_" + type;
        char c = n[n.Length - 1];
        int m = n.Length - (c.L() == 'w' || c.L() == 'v').I();
        Vector3 arrowSlitPos = isRound ? V3.Yz(1.7f, 1.4f) : V3.V(0.3f, 1.7f, 1.25f);
        Material mat = GetMat(mt);
        for (int i = 0; i < m; i++) {
            bool isArrow = n[i].IsU(), isBase = n[i].L() == 'b', isFloor = n[i].L() == 'f', isBase2 = !isBase && !isFloor;
            Poly.CrtPfGo(BldPf("Castle_Tower_" + roundStr + "0" + (isRound ? (isBase || isBase2 ? 1 : 2) : (isBase2 ? 1 : isBase ? 2 : 3)) + liteStr), pTf * Tf.P(V3.Y(i * CastleTowerH)), par2Tf, mat);
            if (isArrow) {
                float dAng = 360f / arrowSlit.Length;
                for (int j = 0; j < arrowSlit.Length; j++)
                    if (arrowSlit[j] != '_') {
                        Poly.CrtPfGo(BldPf("Castle_Arrow_Slit_01"), pTf * Tf.PR(V3.Y(i * CastleTowerH), j * dAng) * Tf.P(arrowSlitPos), par2Tf, mat);
                        if (!isRound)
                            Poly.CrtPfGo(BldPf("Castle_Arrow_Slit_01"), pTf * Tf.PR(V3.Y(i * CastleTowerH), j * dAng) * Tf.P(arrowSlitPos.X(-arrowSlitPos.x)), par2Tf, mat);
                    }
            }
        }
        if (c.L() == 'w' || c.L() != 'v')
            Poly.CrtPfGo(BldPf("Castle_Tower_" + roundStr + "Top_01" + snowStr, type == Ct.Snow), pTf * Tf.P(V3.Y(m * CastleTowerH - (isRound ? 0.05f : 0.4f))), par2Tf, mat);
        if (c.L() == 'w' || c.L() == 'v') {
            Poly.CrtPfGo(BldPf((c.L() == 'w' ? "Castle_Wood_Battlement_01" : "Castle_Roof_Spire_01") + snowStr, type == Ct.Snow), pTf * Tf.P(V3.Y(m * CastleTowerH)), par2Tf, mat);
            if (c == 'W' || c == 'V') {
                Poly.CrtPfGo(BldPf("Castle_Flagpole_01"), pTf * Tf.P(V3.Y(m * CastleTowerH + (c == 'W' ? 2.6f : 5))), par2Tf, mat);
                Poly.CrtPfGo(BldPf("Castle_Flag_01"), pTf * Tf.P(V3.Y(m * CastleTowerH + (c == 'W' ? 2.6f : 5) + 2.4f)), par2Tf, mat);
            }
        }
    }
    // Wall_Gate_01 [d = Door, i = Iron_Gate], Wall_01
    // u = Wood_Battlement_04, v = Wood_Battlement_02, w = Wood_Battlement_03
    // n = Tower_Mini_01, m = Tower_Mini_02
    // [Door][Battlement][Mini][Floor]
    public static void CrtCastleWall(Transform parTf, Vector3 p, float ry, string data, Ct type = Ct.Block, Mat mt = Mat._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("CastleWall", pTf, parTf);
        char door = data[0], btl = data[1], mini = data[2];
        int n = data.Sub(3).I();
        string liteStr = type == Ct.Lite ? "_Lite" : "", snowStr = type == Ct.Block ? "" : "_" + type, snow2Str = type == Ct.Snow ? "_Snow" : "";
        Material mat = GetMat(mt);
        if (door.L() == 'd' || door.L() == 'i')
            Poly.CrtPfGo(BldPf(door.L() == 'd' ? "Castle_Door_01" + snow2Str : "Castle_Iron_Gate_01", door.L() == 'd' && type == Ct.Snow), pTf * Tf.PR(V3.Z(door.L() == 'd' ? door.IsU().Sign() * 1.3f : 0), door.L() == 'd' && door.IsL() ? 180 : 0), par2Tf, mat);
        for (int i = 0; i < n; i++)
            Poly.CrtPfGo(BldPf("Castle_Wall_" + (i == 0 && (door.L() == 'd' || door.L() == 'i') ? "Gate_" : "") + "01" + liteStr), pTf * Tf.P(V3.Y(i * CastleWallH)), par2Tf, mat);
        Poly.CrtPfGo(BldPf("Castle_Tower_Wall_Top_01" + snowStr, type == Ct.Snow), pTf * Tf.P(V3.Y(n * CastleWallH)), par2Tf, mat);
        if (btl.L() == 'u' || btl.L() == 'v' || btl.L() == 'w')
            Poly.CrtPfGo(BldPf("Castle_Wood_Battlement_0" + (btl.L() == 'u' ? 4 : btl.L() == 'v' ? 2 : 3) + snowStr, type == Ct.Snow), pTf * Tf.PR(V3.Yz(2.75f, btl.IsU().Sign() * 1.05f), btl.IsL() ? 180 : 0), par2Tf, mat);
        if (mini.L() == 'n' || mini.L() == 'm')
            Poly.CrtPfGo(BldPf("Castle_Tower_Mini_0" + (mini.L() == 'n' ? 2 : 1) + snow2Str), pTf * Tf.P(V3.Yz(n * CastleWallH + 1, mini.IsU().Sign() * 1.1f)), par2Tf, mat);
    }
    public static Material GetMat(Mat mt) { return A.Load<Material>("PolygonKnights/Materials/PolyKnights_Mat" + mt); }
    public static Material GetMatChr(MatChr mt) { return A.Load<Material>("PolygonKnights/Materials/PolyKnights_Character_Mat_" + mt); }
    public static GameObject BldPf(string s, bool isSnow = false) { return A.LoadGo("PolygonKnights/Prefabs/Buildings/" + (isSnow ? "Snow/" : "") + "SM_Bld_" + s); }
    public static GameObject EnvPf(string s, bool isSnow = false) { return A.LoadGo("PolygonKnights/Prefabs/Environments/" + (isSnow ? "Snow/" : "") + "SM_Env_" + s); }
}