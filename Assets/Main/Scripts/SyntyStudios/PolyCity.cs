using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using Polygon;

public class PolyCity : Mb {
    public enum Car { Ambo, Medium, Muscle, Police, Sedan, Small, Taxi, Van }
    public enum Tile {
        Road_01, Road_02, Road_03, Road_Arrow_01, Road_Arrow_02, Road_Bare_01, Road_Crossing_01, Road_Lines_01, Road_Lines_02, Road_Median_01, Road_Median_02, Road_ParkingLines_01, Road_YellowLines_01, Road_YellowLines_02,
        Sidewalk_01, Sidewalk_Construction_01, Sidewalk_Corner_01, Sidewalk_Corner_02, Sidewalk_Dip_01, Sidewalk_Dip_Corner_01, Sidewalk_Grate_01, Sidewalk_Grate_02, Sidewalk_Gutter_01, Sidewalk_Panel_01, Sidewalk_Panel_02, Sidewalk_Straight_01, Sidewalk_Straight_02,
        Grass_01, GrassPath_Corner_01, GrassPath_Junction_01, GrassPath_Straight_01, GrassPath_T_01, Bridge_Edge_01, Bridge_Underside_01
    }
    public enum Building { OfficeOld_Small_01, OfficeOld_Small_02, OfficeOld_Large_01, OfficeOld_Large_02, CityHall_01 }
    public enum Office { Old_Small, Old_Large, Square, Square_2, Octagon, Round, Round_2 }
    public enum StationSign { None, FireDepartment, Hospital, Police }
    public enum ShopSign { None, Bar, Barber, Cafe, Chinese_Noodles, DeliPizza, Hotel, Pizza, Pub, XXX }
    public enum LargeSign { None, Beer, Bottle, Bowling, Burger, Coffee, Donut, Guitar, Hotdog, Icecream, Lollypop, Milkshake, Noodles, Pizza, Popcorn, Soda, Taco }
    public enum RoadJunction { Road, Crosswalk, Sidewalk }
    public enum Mat { _01_A, _01_B, _01_C, _02_A, _02_B, _02_C, _03_A, _03_B, _03_C, _04_A, _04_B, _04_C }
    public enum MatRoad { _01, _02, _03, _04 }
    public enum Oab { OO, OA, AO, AA, OB, BO, BB, AB, BA }
    public enum Oaa { OO, AO, OA, AA }
    public enum Onp { O, N, P }
    public static Dictionary<Tile, GameObject> TilePfDic = new Dictionary<Tile, GameObject>() { };
    public static List<PolyChr> PolyChrs = List(PolyChr.PolygonCity_Character_BusinessMan_Shirt_01, PolyChr.PolygonCity_Character_BusinessMan_Suit_01, PolyChr.PolygonCity_Character_BusinessWoman_01, PolyChr.PolygonCity_Character_Female_Coat_01, PolyChr.PolygonCity_Character_Female_Jacket_01, PolyChr.PolygonCity_Character_Female_Police_01, PolyChr.PolygonCity_Character_Male_Hoodie_01, PolyChr.PolygonCity_Character_Male_Jacket_01, PolyChr.PolygonCity_Character_Male_Police_01);
    public static List<(Tile, float)> Road2 = List((Tile.Sidewalk_Straight_01, 90f), (Tile.Road_01, 180), (Tile.Road_01, 0), (Tile.Sidewalk_Straight_01, -90)),
        RoadCross2 = List((Tile.Sidewalk_Dip_01, 90f), (Tile.Road_Crossing_01, 90), (Tile.Road_Crossing_01, 90), (Tile.Sidewalk_Dip_01, -90)),
        Road4 = List((Tile.Sidewalk_Straight_01, 90f), (Tile.Road_01, 180), (Tile.Road_YellowLines_02, 0), (Tile.Road_YellowLines_02, 180), (Tile.Road_01, 0), (Tile.Sidewalk_Straight_01, -90)),
        RoadCross4 = List((Tile.Sidewalk_Dip_01, 90f), (Tile.Road_Crossing_01, 90), (Tile.Road_Crossing_01, 90), (Tile.Road_Crossing_01, 90), (Tile.Road_Crossing_01, 90), (Tile.Sidewalk_Dip_01, -90));
    public static float Sz = 5;
    public static List<string> Park3x3 = List(".-. .-.", "|0o║o0|", "._* o_.", " ═ ╬ ═ ", ".~o *~.", "|0o║o0|", ".-. .-."),
        Park5x3 = List(".-.-. .-.-.", "|0 0 ║ 0 0|", ".o*_o o_*o.", " ═ ═ ╬ ═ ═ ", ".o ~o o~ o.", "|0 0 ║ 0 0|", ".-.-. .-.-."),
        Park5x5 = List(".-.-. .-.-.", "|#o#[║]#o#|", ".o o  /o o.", "|#o╔ ╩ ╗o#|", "._* o=o  _.", " ═ ╣+0+╠ ═ ", ".~  o=o *~.", "|#o╚ ╦ ╝o#|", ".o o/  o o.", "|#o#[║]#o#|", ".-.-. .-.-."),
        Park7x3 = List(".-. .-.-.-. .-.", "|0o║ 0 0 0 ║o0|", ".o_ *_o=o_* _o.", " ═ ╬ ═ ═ ═ ╬ ═ ", ".o~ o~o=o~o ~o.", "|0o║ 0 0 0 ║o0|", ".-. .-.-.-. .-."),
        Park7x5 = List(".-.-. .-.-.-. .-.-.", "|0 0 ║ 0 0 0 ║ 0 0|", ". o   * o o *   o .", "|0 0 ╠ ═ ═ ═ ╣ 0 0|", ". *_   =o o=   _* .", " ═ ═ ╣ 0 0 0 ╠ ═ ═ ", ".  ~   =o o=   ~  .", "|0 0 ╠ ═ ═ ═ ╣ 0 0|", ". o   * o o *   o .", "|0 0 ║ 0 0 0 ║ 0 0|", ".-.-. .-.-.-. .-.-.");
    public static GameObject CrtRndCar(Transform parTf, Vector3 p, float ry) {
        return CrtCarRndMat(parTf, p, ry, Rnd.Enum<Car>());
    }
    public static GameObject CrtCarRndMat(Transform parTf, Vector3 p, float ry, Car car) {
        return CrtCar(parTf, p, ry, car, car != Car.Ambo && car != Car.Police && car != Car.Taxi ? Rnd.Enum<Mat>() : Mat._01_A);
    }
    public static GameObject CrtCar(Transform parTf, Vector3 p, float ry, Car car, Mat mt = Mat._01_A) {
        Material mat = GetMat(mt);
        GameObject carGo = Poly.CrtPfGo(VehPf("Car_" + car + "_01"), Tf.PR(p, ry), parTf, mat);
        if (car == Car.Ambo || car == Car.Van) {
            carGo.ChildGo(0).RenMat(mat);
            carGo.ChildGo(1).RenMat(mat);
        }
        return carGo;
    }
    public static void TilePfDicInit() {
        TilePfDic.Clear();
        A.EnumArr<Tile>().List().ForEach(x => TilePfDic.Add(x, EnvPf(x.tS())));
    }
    public static void CrtBuilding(Transform parTf, Vector3 p, float ry, Building building, bool isL = true, Mat mt = Mat._01_A) {
        Poly.CrtPfGo(BldPf(building.tS()), Tf.PR(p, ry + (isL ? 90 : -90)) * Tf.P(building == Building.CityHall_01 ? V3.Xz(isL ? -10 : 10, -10) : isL ? V3.O : building.tS().Contains("Small") ? V3.X(10) : building.tS().Contains("Large") ? V3.X(15) : V3.O), parTf, GetMat(mt));
    }
    public static void CrtStation(Transform parTf, Vector3 p, float ry, StationSign stationSign = StationSign.None, bool isStation1 = true, bool isStationL = false, bool isStationB = false, bool isStationR = false, bool isL = true, Mat mt = Mat._01_A) {
        Tf pTf = Tf.PR(p, ry + (isL ? 90 : -90)) * Tf.P(V3.Xz(isL ? (isStationL ? -15 : -5) : (isStationR ? 15 : 5), -5));
        Transform par2Tf = Poly.NewGoTf("Station", pTf, parTf);
        Material mat = GetMat(mt);
        if (stationSign != StationSign.None)
            Poly.CrtPfGo(PrpPf("Sign_" + stationSign + "_01"), pTf * Tf.P(V3.Yz(5, 3.95f)), par2Tf, mat);
        Poly.CrtPfGo(BldPf("Station_0" + (isStation1 ? 1 : 2)), pTf * Tf.PR(V3.O, 0), par2Tf, mat);
        if (isStationL)
            Poly.CrtPfGo(BldPf("Station_03"), pTf * Tf.P(V3.X(9)), par2Tf, mat);
        if (isStationB)
            Poly.CrtPfGo(BldPf("Station_03"), pTf * Tf.PR(V3.Z(-9), 90), par2Tf, mat);
        if (isStationR)
            Poly.CrtPfGo(BldPf("Station_03"), pTf * Tf.P(V3.X(-9)), par2Tf, mat);
    }
    public static void CrtSideWalk(Transform parTf, Vector3 p, float ry, float lenX, float lenZ, bool isL = true, Mat mt = Mat._01_A) {
        Transform par2Tf = Poly.NewGoTf("SideWalk", Tf.PR(p, ry), parTf);
        Material mat = GetMat(mt);
        for (int i = 0, x = M.RoundI(lenX / Sz), z = M.RoundI(lenZ / Sz); i < z; i++)
            for (int j = 0; j < x; j++)
                CrtTile(par2Tf, p, V3.Xz((!isL).Sign() * (j + 0.5f), i + 0.5f) * Sz, ry, (Tile.Sidewalk_01, 0), mat);
    }
    public static void CrtWaterEdge(Transform parTf, Vector3 p, float ry, bool isFlip, string data, Mat mt = Mat._01_A) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("WaterEdge", pTf, parTf);
        GameObject s1Pf = EnvPf("WaterEdge_Straight_01"), s2Pf = EnvPf("WaterEdge_Straight_02"), s3Pf = EnvPf("WaterEdge_Straight_03"), c1Pf = EnvPf("WaterEdge_Corner_01"), c2Pf = EnvPf("WaterEdge_Corner_02"), pPf = EnvPf("WaterEdge_Pipe_01"), rPf = EnvPf("WaterEdge_Rock_01");
        Vector3 dp = V3.O;
        int f1 = isFlip.I(), f2 = (!isFlip).I();
        bool isC1 = false, isC2 = false;
        string[] datas = data.L().Split(' ');
        Material mat = GetMat(mt);
        for (int i = 0, cnt = 0; i < datas.Length; i++) {
            int n = M.RoundI(datas[i].Sub(1).F() / Sz), d1 = Poly.DirI(datas[i][0]), rot = 180 + d1 * 90 + f1 * 180, d2 = i == datas.Length - 1 ? -1 : Poly.DirI(datas[i + 1][0]);
            Vector3 dir = Poly.DirV3(d1) * Sz;
            int[] dirs = Arr(d1, d2);
            isC2 = dirs[f1] == (dirs[f2] + 1) % 4;
            if (0 < i && i < datas.Length - 1) {
                for (int j = 0; j < n; j++, cnt++) {
                    dp += dir / 2;
                    if (!(isC1 && (j == n - 1 || j == 0))) {
                        // Water Edge Straight
                        Poly.CrtPfGo(cnt > 0 && cnt.IsMod(3) ? s1Pf : Rnd.B ? s2Pf : s3Pf, pTf * Tf.PR(Tf.Pnt(Tf.PR(dp, rot), V3.X(-Sz / 2f)), rot), par2Tf, mat);
                        // Water Edge Pipe
                        if (cnt > 0 && cnt.IsMod(6))
                            Poly.CrtPfGo(pPf, pTf * Tf.PR(Tf.Pnt(Tf.PR(dp, rot), V3.Yz(-2.5f, -1.5f)), rot), par2Tf, mat);
                    }
                    if (j == 0)
                        isC1 = dirs[f2] == (dirs[f1] + 1) % 4;
                    dp += dir / 2;
                }
            } else
                isC1 = dirs[f2] == (dirs[f1] + 1) % 4;
            // Water Edge Corner
            if (i == 0 ? d1 >= 0 : i == datas.Length - 1 ? false : i == datas.Length - 2 ? d2 >= 0 : isC1 || isC2)
                Poly.CrtPfGo(isC1 ? c1Pf : c2Pf, pTf * Tf.PR(dp, rot + (!isFlip == isC1).I() * 90), par2Tf, mat);
        }
    }
    public static void CrtLightPole(Transform parTf, Vector3 p, float ry, bool isLights1 = false, bool isLights2 = false, bool isCrossLights = false, bool isCrossButton = false, bool isBox = false, bool isArm = false, Mat mt = Mat._01_A) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("LightPole", pTf, parTf);
        Material mat = GetMat(mt);
        Poly.CrtPfGo(PrpPf("LightPole_Base_01"), pTf, par2Tf, mat);
        if (isLights1)
            Poly.CrtPfGo(PrpPf("LightPole_Lights_01"), pTf, par2Tf, mat);
        if (isLights2)
            Poly.CrtPfGo(PrpPf("LightPole_Lights_02"), pTf, par2Tf, mat);
        if (isCrossLights)
            Poly.CrtPfGo(PrpPf("LightPole_CrossLights_01"), pTf, par2Tf, mat);
        if (isCrossButton)
            Poly.CrtPfGo(PrpPf("LightPole_CrossButton_01"), Tf.PR(p, ry + 180), par2Tf, mat);
        if (isBox)
            Poly.CrtPfGo(PrpPf("LightPole_Box_01"), pTf, par2Tf, mat);
        if (isArm)
            Poly.CrtPfGo(PrpPf("LightPole_Arm_01"), pTf, par2Tf, mat);
    }
    public static void CrtPark(Transform parTf, Vector3 p, float ry, float lenX, float lenZ, bool isL = true, Mat mt = Mat._01_A) {
        int x = M.RoundI(lenX / Sz), z = M.RoundI(lenZ / Sz), nx = x * 2 + 1, nz = z * 2 + 1, treeCnt = M.RoundI(x * z * 1), wx = x / 2 * 2 + 1, wz = z / 2 * 2 + 1;
        string s1 = "", s2 = "", s3 = "", s4 = "";
        List<string> data = new List<string>();
        for (int i = 0; i < nx; i++) {
            s1 += i.IsEven() ? '.' : i == wx ? ' ' : '-';
            s2 += i == 0 || i == nx - 1 ? '|' : i.IsEven() ? ' ' : '#';
            s3 += i == 0 || i == nx - 1 ? '.' : ' ';
            s4 += i.IsEven() ? ' ' : '═';
        }
        for (int i = 0; i < nz; i++)
            data.Add(i == 0 || i == nz - 1 ? s1 : i.IsEven() ? s3 : i == wz ? s4 : s2);
        for (int i = 1; i < nx; i += 2)
            for (int j = 1; j < nz; j += 2)
                if (data[j][i] == '#')
                    data[j] = data[j].Sub(0, i) + '0' + data[j].SubLast(i + 1, 0);
        for (int i = 1; i < nz; i += 2)
            data[i] = data[i].Sub(0, wx) + (data[i][wx] == '═' ? '╬' : '║') + data[i].SubLast(wx + 1, 0);
        CrtPark(parTf, p, ry, data, isL, mt);
    }
    public static void CrtPark(Transform parTf, Vector3 p, float ry, List<string> parkData, bool isL = true, Mat mt = Mat._01_A) {
        p = Tf.Pnt(Tf.PR(p, ry), V3.Z(isL ? 0 : (parkData[0].Length - 1) / 2 * Sz));
        ry += isL ? -90 : 90;
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Park", pTf, parTf);
        Material mat = GetMat(mt);
        for (int i = 0; i < parkData.Count; i++) {
            for (int j = 0; j < parkData[i].Length; j++) {
                char c = parkData[i][j];
                Vector3 dp = V3.Xz((j - 1) / 2f + 0.5f, (parkData.Count - i) / 2f - 0.5f) * Sz;
                if (c == '#' || c == '0')
                    CrtTile(par2Tf, p, dp, ry, (Tile.Grass_01, 0), mat);
                if (c == '║' || c == '═')
                    CrtTile(par2Tf, p, dp, ry, (Tile.GrassPath_Straight_01, c == '═' ? 90 : 0), mat);
                if (c == '╝' || c == '╚' || c == '╔' || c == '╗')
                    CrtTile(par2Tf, p, dp, ry, (Tile.GrassPath_Corner_01, c == '╝' ? 0 : c == '╚' ? 90 : c == '╔' ? 180 : 270), mat);
                if (c == '╩' || c == '╠' || c == '╦' || c == '╣')
                    CrtTile(par2Tf, p, dp, ry, (Tile.GrassPath_T_01, c == '╩' ? 0 : c == '╠' ? 90 : c == '╦' ? 180 : 270), mat);
                if (c == '╬')
                    CrtTile(par2Tf, p, dp, ry, (Tile.GrassPath_Junction_01, 0), mat);
                if (c == '.')
                    Poly.CrtPfGo(EnvPf("Fence_End_01"), pTf * Tf.P(dp), par2Tf, mat);
                if (c == '|' || c == '-')
                    Poly.CrtPfGo(EnvPf("Fence_01"), pTf * Tf.PR(dp, c == '|' ? 90 : 0) * Tf.P(V3.X(-Sz / 2f)), par2Tf, mat);
                if (c == 'o' || c == '0')
                    Poly.CrtPfGo(EnvPf("Tree_0" + Rnd.RngIn(1, 3)), pTf * Tf.PRS(dp + V3.Xz(Rnd.Rng(-1f, 1f), Rnd.Rng(-1f, 1f)), Q.Y(Rnd.Ang), V3.V(Rnd.Rng(0.6f, 1.2f))), par2Tf, mat);
                if (c == '[' || c == ']' || c == '_' || c == '~')
                    Poly.CrtPfGo(PrpPf("ParkBench_01"), pTf * Tf.PR(dp + V3.Xz(c == '[' ? 1 : c == ']' ? -1 : 0, c == '~' ? 1 : c == '_' ? -1 : 0), c == '~' ? 0 : c == '[' ? 90 : c == '_' ? 180 : 270), par2Tf, mat);
                if (c == '=' || c == '+')
                    Poly.CrtPfGo(PrpPf("PicnicTable_01"), pTf * Tf.PR(dp, c == '=' ? 0 : 90), par2Tf, mat);
                if (c == '*' || c == '/')
                    Poly.CrtPfGo(PrpPf("LightPole_Base_02"), pTf * Tf.PR(dp, c == '*' ? 0 : 90), par2Tf, mat);
            }
        }
    }
    public static void CrtRndApartments(Transform parTf, Vector3 p, float ry, float len, bool isL = true, bool isStair = false) {
        for (int i = 0, z = 0, n = M.RoundI(len / Sz) - (isStair ? 2 : 0); i < n; i++, z += isL ? (isStair ? (i == 1 ? 10 : i == n - 1 ? 15 : 5) : (i == n - 1 ? 10 : 5)) : (isStair ? (i == 1 ? 15 : i == n - 1 ? 10 : 5) : (i == 1 ? 10 : 5))) {
            bool isShop = Rnd.P(0.5f);
            CrtApartment(parTf, Tf.Pnt(Tf.PR(p, ry), V3.Z(z)), ry + (isL ? (i == n - 1 ? 0 : 90) : (i == 0 ? 180 : -90)), Rnd.RngIn(4, 7), Rnd.Enum<Oab>(), Rnd.Enum<ShopSign>(), Rnd.P(0.7f) ? LargeSign.None : Rnd.Enum<LargeSign>(), Rnd.P(0.4f) ? isShop ? Rnd.N(5) : Rnd.N(2) : 0, isStair ? Rnd.RngIn(1, 3) : 0, Rnd.RngIn(1, 6), Rnd.RngIn(1, 3), Rnd.RngIn(1, 3), i == 0 || i == n - 1, isShop, Rnd.P(0.3f), Rnd.P(0.4f), Rnd.Enum<Mat>());
        }
    }
    public static void CrtApartment(Transform parTf, Vector3 p, float ry, int floorCnt = 1, Oab atmPoster = Oab.OO, ShopSign shopSign = ShopSign.None, LargeSign largeSign = LargeSign.None, int cover = 0, int stair = 0, int door = 1, int floor = 1, int roof = 1, bool isCorner = false, bool isShop = false, bool isFireEscape = false, bool isRoofAccess = false, Mat mt = Mat._01_A) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Apartment", pTf, parTf);
        cover = cover == 0 ? 0 : isShop ? (isCorner ? 5 : cover == 5 ? 4 : (cover - 1) % 4 + 1) : (cover - 1) % 2 + 1;
        stair = stair == 0 ? 0 : isCorner ? 1 : (stair - 1) % 3 + 1;
        door = (isShop && isCorner) || !isShop ? (door - 1) % 2 + 1 : (door = (door - 1) % 6 + 1) == 3 ? 2 : door;
        floor = (floor - 1) % 3 + 1;
        roof = (roof - 1) % 3 + 1;
        Vector3 dp = V3.V(isCorner && stair > 0 ? -5 : 0, stair == 1 || stair == 2 ? 1.5f : stair == 3 ? -1.5f : 0, stair > 0 ? -5 : 0);
        string cornerStr = isCorner ? "Corner_" : "", atmPosterStr = atmPoster.tS();
        Material mat = GetMat(mt);
        // Cover
        if (cover > 0)
            Poly.CrtPfGo(BldPf((isShop ? "Shop_" : "") + "Cover_0" + cover), pTf * Tf.P(isShop ? (dp + V3.V(isCorner ? 0 : door == 3 ? -Sz : -Sz / 2f, 3.5f, Arr(1.4f, 1.6f, 1.7f, 0, 0)[cover - 1])) : dp + V3.V(-Sz / 2f, 3.5f, cover == 2 && door == 1 ? 0.2f : 0)), par2Tf, mat);
        // Stair
        if (stair > 0)
            Poly.CrtPfGo(BldPf("Apartment_Stairs_" + cornerStr + "0" + stair), pTf * Tf.PR(V3.Z(stair > 0 && isCorner ? -10 : 0), stair > 0 && isCorner ? 90 : 0), par2Tf, mat);
        // ATM Poster
        if (door > 0 && !isShop) {
            Vector3[,] apDatas = new Vector3[,] { { V3.V(-0.6f, 1.5f, 0.3f), V3.V(-0.9f, 1.5f, 0.3f) }, { V3.V(-0.6f, 1, 0.1f), V3.V(-0.9f, 1, 0.1f) } };
            for (int i = 0; i < 2; i++) {
                bool isPoster = !((stair == 0 || stair == 3) && atmPosterStr[i] == 'A');
                Vector3 pos = apDatas[isPoster.I(), door - 1], pos2 = (i == 0 ? pos : pos.X(-pos.x - Sz));
                if (atmPosterStr[i] != 'O') {
                    Poly.CrtPfGo(PrpPf(isPoster ? "Poster_Frame_01" : "ATM_01"), pTf * Tf.P(dp + pos2), par2Tf, mat);
                    if (isPoster)
                        Poly.CrtPfGo(PrpPf("Poster_0" + Rnd.RngIn(1, 3)), pTf * Tf.P(dp + pos2), par2Tf, mat);
                }
            }
        }
        // Shop Sign
        if (isShop && shopSign != ShopSign.None) {
            bool isBarber = shopSign == ShopSign.Barber, isDeliPizza = shopSign == ShopSign.DeliPizza, isHotel = shopSign == ShopSign.Hotel, isAtc2 = isBarber || isDeliPizza, isAtc = isAtc2 || isHotel, isCover = 1 <= cover && cover <= 3;
            Vector3 ssPos = dp + (isAtc ? V3.V(Arr(-0.4f, -0.15f, -0.25f, -0.11f, -0.11f, -0.15f, 0.14f, 0)[door - 1 + (isCorner ? 6 : 0)], 1.4f, Arr(0.32f, 0.24f, 0.4f, 0.68f, 0, 0.27f, 0.07f, -4.88f)[door - 1 + (isCorner ? 6 : 0)]) : V3.V(-Sz / 2f, door <= 2 ? 2.55f : 2.73f, Arr(0.65f, 0.52f, 0.6f, 0.95f, 0.2f, 0.9f, 0.36f, 0.27f)[door - 1 + (isCorner ? 6 : 0)]));
            if (isAtc ? !isBarber : !isCover)
                Poly.CrtPfGo(PrpPf("Sign_Attachment_0" + (isAtc ? 2 : 3)), isAtc ? pTf * Tf.PR(isDeliPizza ? ssPos : V3.Xy(-0.7f, 6.5f), isAtc2 && isCorner ? 90 : 0) : pTf * Tf.P(ssPos) * Tf.P(V3.Z(-1.22f)), par2Tf, mat);
            Poly.CrtPfGo(PrpPf("Sign_" + shopSign + "_01"), isAtc2 ? pTf * Tf.PR(ssPos, isCorner ? 90 : 0) * Tf.PR(isBarber ? V3.Y(-0.5f) : V3.Z(0.5f), isDeliPizza ? 90 : 0) : isHotel ? pTf * Tf.P(dp + V3.V(-1, 4.5f, 0.5f)) : pTf * Tf.P(isCover ? dp + V3.V(door == 3 ? -Sz : -Sz / 2f, Arr(3.5f, 3.2f, 3.03f)[cover - 1], Arr(2.8f, 3.22f, 3.45f)[cover - 1]) : ssPos) * Tf.P(V3.Y(Arr(0, -0.335f, 0, 0, 0, 0, -0.225f, -0.33f, 0, -0.35f, -0.28f, -0.275f, -0.475f)[(int)shopSign])), par2Tf, mat);
        }
        // Large Sign
        if (largeSign != LargeSign.None && !isFireEscape) {
            Vector3[,] lsDatas = new Vector3[,] {
                { V3.Z(1), V3.Z(30) }, { V3.Z(0.9f), V3.Z(30) }, { V3.O, V3.O }, { V3.Z(1.8f), V3.Z(30) }, { V3.V(0.3f, -0.6f, 0.8f), V3.Z(30) }, { V3.Z(0.7f), V3.X(90) }, { V3.V(0.7f, -1.2f, 0.2f), V3.Z(30) }, { V3.O, V3.V(60, 90, 90) }, { V3.Z(0.7f), V3.Xz(15, 30) }, { V3.Z(0.2f), V3.Z(30) }, { V3.Z(0.8f), V3.Z(30) }, { V3.V(0.3f, -1, 0.9f), V3.Xz(10, 30) }, { V3.Y(0.5f), V3.X(90) }, { V3.Yz(0.1f, 0.9f), V3.Xz(5, 30) }, { V3.Z(1), V3.Z(30) }, { V3.V(0.3f, -0.6f, 0.6f), V3.Xy(-30, 90) }, { V3.O, V3.O } };
            Poly.CrtPfGo(PrpPf("Sign_Attachment_03"), pTf * Tf.P(dp + V3.V(-2.5f, 6, -0.5f)), par2Tf, mat);
            if (largeSign == LargeSign.Bowling)
                for (int i = 0; i < 4; i++)
                    Poly.CrtPfGo(PrpPf("LargeSign_Bowling" + (i == 0 ? "Ball" : "Pin") + "_01"), pTf * Tf.P(dp + V3.V(-2.5f, 6, 0.5f)) * Tf.PR(Arr(V3.Z(1.8f), V3.V(1.5f, 2.6f, 0.8f), V3.V(-0.5f, 2.4f, 0.8f), V3.V(-2.5f, 1.3f, 0.8f))[i], V3.Z(Arr(0, 0, 20, 60)[i])), par2Tf, mat);
            else
                Poly.CrtPfGo(PrpPf("LargeSign_" + largeSign + "_01"), pTf * Tf.P(dp + V3.V(-2.5f, 6, 0.5f)) * Tf.PR(lsDatas[(int)largeSign - 1, 0], lsDatas[(int)largeSign - 1, 1]), par2Tf, mat);
        }
        // Apartment
        Poly.CrtPfGo(BldPf((isShop ? "Shop_" : "Apartment_Door_") + cornerStr + "0" + door), pTf * Tf.P(dp), par2Tf, mat);
        for (int i = 1; i < floorCnt; i++)
            Poly.CrtPfGo(BldPf("Apartment_" + cornerStr + "0" + floor), pTf * Tf.P(dp + V3.Y(i * 3)), par2Tf, mat);
        Poly.CrtPfGo(BldPf("Apartment_Roof_" + cornerStr + "0" + roof), pTf * Tf.P(dp + V3.Y(floorCnt * 3)), par2Tf, mat);
        // Fire Escape
        if (isFireEscape) {
            for (int i = 0, m = isCorner ? 2 : 1; i < m; i++) {
                Vector3 dp2 = dp + V3.Z(i > 0 ? -5 : 0);
                Quaternion rot2 = Q.Y(i > 0 ? 90 : 0);
                if (floorCnt > 2)
                    Poly.CrtPfGo(BldPf("FireEscape_03"), pTf * Tf.PR(dp2 + V3.Y(3), rot2), par2Tf, mat);
                for (int j = 2; j < floorCnt - 1; j++)
                    Poly.CrtPfGo(BldPf("FireEscape_02"), pTf * Tf.PR(dp2 + V3.Y(j * 3), rot2), par2Tf, mat);
                if (floorCnt > 1)
                    Poly.CrtPfGo(BldPf("FireEscape_01"), pTf * Tf.PR(dp2 + V3.Y((floorCnt - 1) * 3), rot2), par2Tf, mat);
            }
        }
        // Roof Access
        if (isRoofAccess)
            Poly.CrtPfGo(BldPf("Roof_Access_01"), pTf * Tf.P(dp + V3.V(-2.5f, 0.5f + floorCnt * 3, -2.5f)), par2Tf, mat);
    }
    public static void CrtRndOffices(Transform parTf, Vector3 p, float ry, float len, bool isL = true, bool isSideWalk = false, Transform sideWalkParTf = null, Mat mt = Mat._01_A) {
        Dictionary<int, List<string>> ofcDic = new Dictionary<int, List<string>>() { { 8, List("3 4", "4 3", "2 2 2") }, { 7, List("2 4", "3 3", "4 2") }, { 6, List("2 3", "3 2") }, { 5, List("2 2") }, { 4, List("4") }, { 3, List("3") }, { 2, List("2") } };
        int m = M.RoundI(len / Sz), i = 0, max = 0;
        while (i <= m) {
            int[] datas = ofcDic.ContainsKey(m - i) ? Rnd.List(ofcDic[m - i]).IntArr() : Arr(Rnd.RngIn(2, 4));
            for (int j = 0; j < datas.Length; j++) {
                CrtOffice(parTf, Tf.Pnt(Tf.PR(p, ry), V3.Z((i + (isL ? 0 : datas[j])) * Sz)), ry + (isL ? 90 : -90), datas[j] == 2 ? Office.Old_Small : datas[j] == 3 ? Rnd.Arr(Office.Old_Large, Office.Square, Office.Square_2) : Rnd.Arr(Office.Octagon, Office.Round, Office.Round_2), Rnd.RngIn(5, 19), Rnd.RngIn(1, 2), Rnd.Enum<Mat>());
                i += datas[j] + 1;
                max = max < datas[j] ? datas[j] : max;
            }
        }
        // Side Walk
        if (isSideWalk)
            CrtSideWalk(sideWalkParTf.Null() ? parTf : sideWalkParTf, p, ry, M.RoundI(Sz * max), len, isL, mt);
    }
    public static void CrtOffice(Transform parTf, Vector3 p, float ry, Office office, int floorCnt = 1, int floor = 1, Mat mt = Mat._01_A) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Office", pTf, parTf);
        string ofcStr = office.tS().Replace("_2", ""), floorStr = ofcStr == "Square" || ofcStr == "Round" ? office.tS().IsE("_2") ? "04" : "02" : "Floor_01";
        float baseH = ofcStr.IsS("Old") ? 6 : ofcStr == "Square" ? 3.52f : ofcStr == "Octagon" ? 3.89f : 6.18f, floorH = office == Office.Square_2 ? 7.5f : office == Office.Round_2 ? 8.95f : office == Office.Square || office == Office.Round ? 9 : 3, dz = office == Office.Octagon ? -9.8f : 0;
        Vector3 dp = ofcStr == "Octagon" || ofcStr == "Round" ? V3.Xz(-10, -10) : V3.O;
        floorCnt = ofcStr == "Square" || ofcStr == "Round" ? (floorCnt - 1) / 3 + 1 : floorCnt - 1;
        Material mat = GetMat(mt);
        // Office
        Poly.CrtPfGo(BldPf("Office" + ofcStr + "_Base_01"), pTf * Tf.P(dp), par2Tf, mat);
        for (int i = 0; i < floorCnt - 1; i++)
            Poly.CrtPfGo(BldPf("Office" + ofcStr + "_" + floorStr), pTf * Tf.P(dp.Y(baseH + i * floorH)), par2Tf, mat);
        Poly.CrtPfGo(BldPf("Office" + ofcStr + "_Roof_01"), pTf * Tf.P(dp.Y(baseH + (floorCnt - 1) * floorH)), par2Tf, mat);
    }
    public static void CrtBridges(Transform parTf, Vector3 p, float ry, float len, List<(Tile, float)> types, Mat mt = Mat._01_A, MatRoad mtR = MatRoad._01) {
        for (int i = 0, n = M.RoundI(len / Sz); i < n; i++)
            CrtBridge(parTf, p + Ang.Xz(Ang.Unity(ry), Sz * i), ry, i != 0 && i.IsMod(3), i == 0 ? Onp.N : i == n - 1 ? Onp.P : Onp.O, i == 0 ? Onp.N : i == n - 1 ? Onp.P : Onp.O, types, mt, mtR);
    }
    public static void CrtBridge(Transform parTf, Vector3 p, float ry, bool isSupport, Onp pillar, Onp wall, List<(Tile, float)> types, Mat mt = Mat._01_A, MatRoad mtR = MatRoad._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Bridge", pTf, parTf);
        float d = types.Count.StaD();
        Material mat = GetMat(mt), matR = GetMatRoad(mtR);
        for (int i = 0; i < types.Count; i++) {
            CrtTile(par2Tf, p, V3.Xz(d + i, 0.5f) * Sz, ry, types[i], matR);
            // Bridge Edge, Bridge Underside
            CrtTile(par2Tf, p, V3.Xz(d + i, 0.5f) * Sz, ry, (i == 0 || i == types.Count - 1 ? Tile.Bridge_Edge_01 : Tile.Bridge_Underside_01, i == 0 ? 180 : 0), matR);
        }
        // Bridge Support
        if (isSupport)
            Poly.CrtPfGo(EnvPf("Bridge_Support_01"), pTf * Tf.P(V3.Y(-3.35f)), par2Tf, mat);
        // Bridge Pillar
        if (pillar != Onp.O) {
            Poly.CrtPfGo(EnvPf("Bridge_Pillar_01"), pTf * Tf.PR(V3.Xz(d - 0.5f, pillar == Onp.N ? 0 : 1) * Sz, 180), par2Tf, mat);
            Poly.CrtPfGo(EnvPf("Bridge_Pillar_01"), pTf * Tf.P(V3.Xz(0.5f - d, pillar == Onp.N ? 0 : 1) * Sz), par2Tf, mat);
        }
        // Bridge Wall
        if (wall != Onp.O)
            for (int i = 0; i < types.Count; i++)
                Poly.CrtPfGo(EnvPf("Bridge_Wall_01"), pTf * Tf.PR(V3.Xz((d + i) * Sz, wall == Onp.N ? 1 : Sz - 1), wall == Onp.N ? 180 : 0) * Tf.P(V3.X(-Sz / 2f)), par2Tf, mat);
    }
    public static void CrtRoads(Transform parTf, Vector3 p, float ry, float len, List<(Tile, float)> types, Mat mt = Mat._01_A, MatRoad mtR = MatRoad._01) {
        for (int i = 0, n = M.RoundI(len / Sz); i < n; i++)
            CrtRoad(parTf, p + Ang.Xz(Ang.Unity(ry), Sz * i), ry, i == 0 ? Oaa.OO : i.IsMod(3) ? (i / 3).IsEven() ? Oaa.AO : Oaa.OA : Oaa.OO, types, mt, mtR);
    }
    public static void CrtRoad(Transform parTf, Vector3 p, float ry, Oaa lightPole, List<(Tile, float)> types, Mat mt = Mat._01_A, MatRoad mtR = MatRoad._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("Road", pTf, parTf);
        float d = types.Count.StaD(), x = (d + 0.5f) * Sz - 1;
        Material matR = GetMatRoad(mtR);
        // Light Pole
        if (lightPole != Oaa.OO) {
            if (lightPole == Oaa.AO || lightPole == Oaa.AA)
                CrtLightPole(par2Tf, Tf.Pnt(pTf, V3.X(x)), ry + 90, false, false, false, false, false, false, mt);
            if (lightPole == Oaa.OA || lightPole == Oaa.AA)
                CrtLightPole(par2Tf, Tf.Pnt(pTf, V3.X(-x)), ry - 90, false, false, false, false, false, false, mt);
        }
        // Road
        for (int i = 0; i < types.Count; i++)
            CrtTile(par2Tf, p, V3.Xz(d + i, 0.5f) * Sz, ry, types[i], matR);
    }
    public static void CrtRoadJunction(Transform parTf, Vector3 p, float ry, int n, RoadJunction l, RoadJunction f, RoadJunction r, RoadJunction b, Mat mt = Mat._01_A, MatRoad mtR = MatRoad._01) {
        Tf pTf = Tf.PR(p, ry);
        Transform par2Tf = Poly.NewGoTf("RoadJunction", pTf, parTf);
        Material mat = GetMat(mt), matR = GetMatRoad(mtR);
        float d = n.StaD();
        // Road
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                CrtTile(par2Tf, p, V3.Xz(d + j, i + 1.5f) * Sz, ry, (Tile.Road_Bare_01, 0), matR);
        RoadJunction[] rcs = Arr(l, f, r, b, l);
        Vector3 cen = V3.Z(n / 2f + 1) * Sz;
        for (int i = 0; i < 4; i++) {
            bool isCw = rcs[i] == RoadJunction.Crosswalk, isSw = rcs[i] == RoadJunction.Sidewalk, isCw2 = rcs[i + 1] == RoadJunction.Crosswalk, isSw2 = rcs[i + 1] == RoadJunction.Sidewalk;
            // Light Pole
            if (isCw || !isSw)
                CrtLightPole(par2Tf, Tf.Pnt(pTf, Tf.Pnt(Tf.PR(cen, i * 90), V3.Xz((d - 1.5f) * Sz, n / 2f * Sz + 1))), ry + i * 90 + 180, isCw, isCw, isCw, isCw, true, true, mt);
            // Road Cross
            for (int j = 0; j < n; j++)
                CrtTile(par2Tf, p, Tf.Pnt(Tf.PR(cen, i * 90), V3.Xz(d - 1, n / 2f - j - 0.5f) * Sz), ry,
                    (isCw ? Tile.Road_Crossing_01 : isSw ? Tile.Sidewalk_Straight_01 : n == 1 ? Tile.Road_Bare_01 : 0 < j && j < n - 1 ? Tile.Road_03 : Tile.Road_01,
                    (isCw ? 0 : isSw ? 90 : j == 0 ? -90 : 90) + i * 90), isSw ? mat : matR);
            // Road Corner
            CrtTile(par2Tf, p, Tf.Pnt(Tf.PR(cen, i * 90), V3.Xz(d - 1, n / 2f + 0.5f) * Sz), ry,
                (isSw && isSw2 ? Tile.Sidewalk_Corner_02 : !isSw && !isSw2 ? isCw || isCw2 ? Tile.Sidewalk_Dip_Corner_01 : Tile.Sidewalk_Corner_01 : isCw || isCw2 ? Tile.Sidewalk_Dip_01 : Tile.Sidewalk_Straight_01,
                (isSw && isSw2 ? 90 : !isSw && !isSw2 ? isCw || isCw2 ? 180 : 90 : isSw ? 90 : 180) + i * 90), mat);
        }
    }
    public static void CrtTile(Transform parTf, Vector3 p, Vector3 dp, float ry, (Tile, float) type, Material mat) {
        Poly.CrtTile(TilePfDic[type.Item1], p, ry, dp, type.Item2, !type.Item1.tS().IsS("GrassPath") && !type.Item1.tS().IsS("Bridge") ? Poly.Tile.BL : Poly.Tile.BR, Sz, parTf, mat);
    }
    public static void Chr(PolygonChr chr, PolyChr pc, Mat mat) {
        chr.Chr(pc);
        chr.chrGo.Child(PolyChrs.IndexOf(pc)).RenMat(GetMat(mat));
    }
    public static Material GetMat(Mat mt) { return A.Load<Material>("PolygonCity/Materials/PolygonCity_Mat" + mt); }
    public static Material GetMatRoad(MatRoad mt) { return A.Load<Material>("PolygonCity/Materials/PolygonCity_Mat_Road" + mt); }
    public static GameObject VehPf(string s) { return A.LoadGo("PolygonCity/Prefabs/Vehicles/SM_Veh_" + s); }
    public static GameObject BldPf(string s) { return A.LoadGo("PolygonCity/Prefabs/Buildings/SM_Bld_" + s); }
    public static GameObject EnvPf(string s) { return A.LoadGo("PolygonCity/Prefabs/Environments/SM_Env_" + s); }
    public static GameObject PrpPf(string s) { return A.LoadGo("PolygonCity/Prefabs/Props/SM_Prop_" + s); }
}