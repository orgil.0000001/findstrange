using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Bullet : Mb {
    public int pwr;
    private void OnTriggerEnter(Collider other) {
        if (other.Tag(Tag.Enemy)) {
            other.Par<Enemy>(1).Hit(pwr);
        } else if (!other.Tag(Tag.Player)) {
            if (Player.I.shotCnt == 0) {
                for (int i = 0; i < Ls.I.enemies.Count; i++) {
                    if (Ls.I.enemies[i].isLive) {
                        Ls.I.enemies[i].cGo.Hide();
                        Ins(Ls.I.expPf, Ls.I.enemies[i].Tp, Q.O, Ls.I.enemies[i].tf);
                    }
                }
                Gc.I.GameOver();
            }
            Dst(go);
        }
    }
}
