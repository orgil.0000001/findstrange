using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using UnityEngine.UI;

public class HealthBar : Mb {
    public Image img;
    public Gradient g;
    public Enemy enemy;
    private void Start() {
    }
    void Update() {
        if (enemy) {
            Tp = enemy.Tp;
            Tr = Cm.I.Tr;
            img.color = g.Evaluate(enemy.lifeF);
            img.fillAmount = enemy.lifeF;
        }
    }
    public void AnimShow(bool isShow) {
        go.An().Play(isShow ? "Show" : "Hide");
    }
}
