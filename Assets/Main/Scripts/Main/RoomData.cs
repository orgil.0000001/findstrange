﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class RoomData : Mb {
    public List<OfcWall2Tp> l, r, b, f;
    public OfcFloorTp floorTp = OfcFloorTp.Carpet_01;
    public OfcWallTp wallTp = OfcWallTp.Baseboard;
    public OfcMat floorMat = OfcMat._01_A, wallMat = OfcMat._01_A, ceilMat = OfcMat._01_A;
    public int ceil = 1, light = 1, door = 1;
    private void Start() {
        Es.I.Room(V3.O, l, r, b, f, floorTp, floorMat, wallTp, wallMat, ceil, ceilMat, light, door);
    }
}