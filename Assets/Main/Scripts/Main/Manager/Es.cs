﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public enum OfcFloorTp { Carpet_01, Concrete_01, Grass_01, Tiles_01, Wood_01 }
public enum OfcWallTp { Baseboard, Blank, Brick, Glass, Trim }
public enum OfcWall2Tp { _01, Door_01, Door_Large_01, Elevator_01, Window_01, Window_02, Window_03, Window_04 }
public enum OfcMat { _01_A, _01_B, _01_C, _02_A, _02_B, _02_C, _03_A, _03_B, _03_C, _04_A, _04_B, _04_C }

public class Es : Singleton<Es> { // Environment Spawner
    GameObject doorGo;
    const float sz = 2.5f;
    Vector3 tileDp = V3.Xz(sz / 2, -sz / 2), wallDp = V3.X(sz / 2), wallLDp = V3.X(sz);
    public void Init() {
    }
    public void Door(bool isOpen, float rot, float tm, float waitTm = 0) {
        StaCor(DoorCor(isOpen, rot, tm, waitTm));
    }
    IEnumerator DoorCor(bool isOpen, float rot, float tm, float waitTm = 0) {
        yield return Wf.S(waitTm);
        GameObject lGo = doorGo.ChildGo(0), rGo = doorGo.ChildGo(1);
        float r1 = isOpen ? 0 : rot, r2 = isOpen ? rot : 0;
        for (float t = 0; t < tm; t += Dt) {
            lGo.TleY(M.Lerp(-r1, -r2, t / tm));
            rGo.TleY(M.Lerp(r1, r2, t / tm));
            yield return null;
        }
        lGo.TleY(-r2);
        rGo.TleY(r2);
    }
    public void Room(Vector3 pos,
        List<OfcWall2Tp> l, List<OfcWall2Tp> r, List<OfcWall2Tp> b, List<OfcWall2Tp> f,
        OfcFloorTp floorTp, OfcMat floorMat, OfcWallTp wallTp, OfcMat wallMat, int ceil, OfcMat ceilMat, int light, int door) {
        Transform floorParTf = Crt.NewTf(Tf.N("Floor"), tf), wallParTf = Crt.NewTf(Tf.N("Wall"), tf), ceilParTf = Crt.NewTf(Tf.N("Ceil"), tf);
        GameObject ceilPf = OfcBldPf("Ceiling_" + (ceil >= 3 ? "Panel_0" : "0") + (ceil == 1 || ceil == 3 ? 1 : 2)), floorPf = OfcFloorPf(floorTp), lightPf = light <= 0 ? null : OfcBldPf("Ceiling_Panel_Light_0" + (light == 1 ? 1 : 2));
        Dictionary<OfcWall2Tp, GameObject> wallDic = new Dictionary<OfcWall2Tp, GameObject>() { };
        A.EnumArr<OfcWall2Tp>().List().ForEach(x => wallDic.Add(x, OfcWallPf(wallTp, x)));
        System.Func<List<OfcWall2Tp>, int, bool> j2 = (a, b) => b < a.Count && (a[b] == OfcWall2Tp.Door_Large_01 || a[b] == OfcWall2Tp.Window_03);
        List<List<OfcWall2Tp>> lis = List(b, l, f, r);
        int x = 0, z = 0;
        for (int i = 0, j, k; i < 4; i++) {
            for (j = 0, k = 0; j < lis[i].Count; k += j2(lis[i], j) ? 2 : 1, j++) ;
            x = i == 0 || (k > x && i == 2) ? k : x;
            z = i == 1 || (k > z && i == 3) ? k : z;
        }
        Vector3 sp = pos + V3.Xz(x.StaD(), z.StaD()) * sz;
        System.Func<int, float, Vector3> p2 = (a, b) => sp + (a == 0 ? V3.V(b * sz, 0, -0.5f * sz) : a == 1 ? V3.V(-0.5f * sz, 0, b * sz) : a == 2 ? V3.V(b * sz, 0, (z - 0.5f) * sz) : V3.V((x - 0.5f) * sz, 0, b * sz));
        for (int i = 0; i < z; i++) {
            for (int j = 0; j < x; j++) {
                Vector3 p = sp + V3.Xz(j * sz, i * sz);
                CrtTile(floorParTf, floorPf, p, floorMat, 0, tileDp, true, true);
                CrtTile(ceilParTf, ceilPf, p.Y(3), ceilMat, 0, tileDp, true, true);
                if (lightPf)
                    CrtTile(ceilParTf, lightPf, p.Y(2.99f), OfcMat._01_A, 0, V3.O, true, true);
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0, k = 0, n = i.IsEven() ? x : z; k < n; k += j2(lis[i], j) ? 2 : 1, j++) {
                OfcWall2Tp tp = j < lis[i].Count ? lis[i][j] : OfcWall2Tp._01;
                Vector3 p = p2(i, k + (j2(lis[i], j) ? 0.5f : 0));
                CrtTile(wallParTf, wallDic[tp], p, wallMat, i * 90, j2(lis[i], j) ? wallLDp : wallDp, true);
                if (tp == OfcWall2Tp.Door_01) {
                    if (wallTp != OfcWallTp.Glass)
                        CrtTile(wallParTf, OfcBldPf("Door_0" + (1 <= door && door <= 5 ? door : 1)), p, wallMat, 0, V3.X(0.5f), true);
                } else if (tp == OfcWall2Tp.Door_Large_01) {
                    CrtTile(wallParTf, OfcBldPf("Door_Large_01_R"), p, wallMat, 0, V3.X(-0.95f), true);
                    CrtTile(wallParTf, OfcBldPf("Door_Large_01_L"), p, wallMat, 0, V3.X(0.95f), true);
                }
            }
        }
    }
    public GameObject CrtTile(Transform parTf, GameObject pf, Vector3 pos, OfcMat mat, float rot, Vector3 dp, bool isRmvShadow = false, bool isRmvCol = false, int lyr = Lyr.Wall) {
        GameObject go = Ins(pf, Tf.PR(pos, rot).Pnt(dp), Q.Y(rot), parTf);
        go.RenMat(Mat(mat));
        go.layer = lyr;
        if (isRmvCol)
            Dst(go.Col());
        if (isRmvShadow) {
            go.Mr().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            go.Mr().receiveShadows = false;
        }
        return go;
    }
    string Ul(object o) { return (o.tS().IsS("_") ? "" : "_"); }
    public GameObject OfcFloorPf(OfcFloorTp tp) { return OfcBldPf("Floor", tp); }
    public GameObject OfcWallPf(OfcWallTp tp, OfcWall2Tp tp2) { return OfcBldPf(tp == OfcWallTp.Glass ? (tp2 == OfcWall2Tp.Window_03 ? "Wall_Glass_Large_01" : tp2 == OfcWall2Tp.Door_Large_01 ? "Wall_Glass_Large_Door_01" : tp2 == OfcWall2Tp.Elevator_01 ? "Elevator_01" : tp2 == OfcWall2Tp.Door_01 ? "Wall_Glass_Door_01" : "Wall_Glass_01") : "Wall_" + tp + Ul(tp2) + tp2.tS()); }
    public GameObject OfcBldPf(string name1, object name2) { return OfcBldPf(name1 + Ul(name2) + name2); }
    public GameObject OfcBldPf(string name) { return A.LoadGo("PolygonOffice/Prefabs/Buildings/SM_Bld_" + name); }
    public Material Mat(OfcMat mat) { return A.Load<Material>("PolygonOffice/Materials/" + (mat.tS().IsE("A") ? "" : "Alts/") + "PolygonOffice_Material" + mat); }

    public void ColumnsCol(Material columnMat, Material bgMat, Color columnCol, Color bgUCol, Color bgDCol, Vector2 fogY) {
        columnMat.color = columnCol;
        columnMat.SetFloat("_HeightFogStart", fogY.x);
        columnMat.SetFloat("_HeightFogEnd", fogY.y);
        columnMat.SetColor("_HeightFogColor", bgDCol);
        bgMat.color = bgUCol;
        bgMat.SetColor("_HeightFogColor", bgDCol);
    }
    public void CrtColumns(GameObject columnPf, Transform columnsTf, List<float> xs, float dx, Vector2 y, Vector2 sz, Vector2 spc, Vector2 fogY, float len) {
        for (int i = 0; i < xs.Count; i++)
            for (float z = 0; z < len;) {
                float colSz = sz.Rnd(), colX = xs[i].RndD(dx), colY = y.Rnd();
                GameObject columnGo = Ins(columnPf, V3.V(colX, (colY + fogY.x) / 2f, z + colSz), Q.O, columnsTf);
                columnGo.Tls(V3.V(colSz, colY - fogY.x, colSz));
                z += colSz + spc.Rnd();
            }
    }
    public void CrtBlds(string path, float roadW, float l) {
        List<BoxCollider> bldPfs = Resources.LoadAll<BoxCollider>(path).List();
        for (int i = 0; i < 2; i++) {
            bool isR = i == 1;
            List<GameObject> lis = new List<GameObject>();
            for (int j = 0; (lis.IsEmpty() ? true : lis.Last().Tp().z < l) && j < 100; j++) {
                BoxCollider bc = bldPfs.Rnd();
                float lstZ = lis.IsEmpty() ? 0 : lis.Last().Tp().z + lis.Last().BcSz().x / 2 + lis.Last().BcCen().x * isR.Sign();
                Vector3 pos = V3.V((roadW / 2 + bc.size.z / 2 + bc.center.z) * isR.Sign(), bc.size.y / 2 - bc.center.y, lstZ + bc.size.x / 2 - bc.center.x * isR.Sign());
                GameObject bldGo = Ins(bc.gameObject, pos, Q.Y(isR ? -90 : 90), tf);
                lis.Add(bldGo);
            }
        }
    }
}