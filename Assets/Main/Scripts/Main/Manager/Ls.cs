﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Level {
    public int envTp, shotCnt;
    public List<Vector3> enemies;
    public Level(int envTp, int shotCnt, params Vector3[] enemies) {
        this.envTp = envTp;
        this.shotCnt = shotCnt;
        this.enemies = enemies.List();
    }
}

public class Ls : Singleton<Ls> { // Level Spawner
    public List<RoomData> roomDatas;
    List<Level> levels = new List<Level>() {
        new Level(1, 15, V3.V(1,0,2)),
        new Level(2, 10, V3.V(0.5f,0.9f,-2.4f)),//new Level(2, 10, V3.V(3,0,0)),
        new Level(3, 30, V3.V(0,0,2.5f), V3.V(-1.5f,0,-2)),
        new Level(4, 30, V3.V(-3,0,0), V3.V(1,0.9f,-3.5f)),//new Level(4, 30, V3.V(-3,0,0), V3.V(3,0,-2)),
        new Level(5, 25, V3.V(3,0,0), V3.V(-2.5f,0,4)),

        new Level(1, 45, V3.V(-2.8f,0,0.7f), V3.V(3,0,1.2f), V3.V(-3.3f,0,-0.1f)),
        new Level(2, 40, V3.V(-3,0,-1), V3.V(2.4f,0,1), V3.V(0.7f,0.9f,-2.5f)),//new Level(2, 40, V3.V(-3,0,-1), V3.V(2.4f,0,1), V3.V(0.7f,0,0)),
        new Level(3, 40, V3.V(-1.7f,0,2.8f), V3.V(0,0,2.8f), V3.V(2.2f,0,1)),
        new Level(4, 35, V3.V(0,0.9f,-3.5f), V3.V(3.2f,0,0), V3.V(-1.8f,0,-1)),//new Level(4, 35, V3.V(-2.6f,0,-2), V3.V(3.2f,0,0), V3.V(-1.8f,0,-1)),
        new Level(5, 35, V3.V(4.4f,0,-2.6f), V3.V(2.8f,0,-2.6f), V3.V(4.4f,0,-1)),

        new Level(1, 45, V3.V(-2.6f,0,1.2f), V3.V(-2.6f,0,0), V3.V(2.6f,0,1.2f), V3.V(2.6f,0,0)),
        new Level(2, 40, V3.V(-1.5f,0.9f,-2), V3.V(0,0.9f,-2.5f), V3.V(3.2f,0,-0.8f), V3.V(3.2f,0,-1.3f)),//new Level(2, 40, V3.V(3.2f,0,0.2f), V3.V(3.2f,0,-0.3f), V3.V(3.2f,0,-0.8f), V3.V(3.2f,0,-1.3f)),
        new Level(3, 40, V3.V(-1.5f,0,-3), V3.V(0.8f,0,-3), V3.V(-0.7f,0,2.5f), V3.V(0.7f,0,2.5f)),
        new Level(4, 50, V3.V(-3,0,0), V3.V(-2,0,0), V3.V(-3,0,-3), V3.V(2,0.9f,-3), V3.V(3.3f,0,-2)),//new Level(4, 50, V3.V(-3,0,0), V3.V(-2,0,0), V3.V(-3,0,-3), V3.V(-2,0,-3), V3.V(3.3f,0,-2)),
        new Level(5, 45, V3.V(-2.4f,0,4.3f), V3.V(-1.2f,0,4.3f), V3.V(0,0,4.3f), V3.V(1.2f,0,4.3f), V3.V(2.4f,0,4.3f)),
    };
    public bool useLvl = false;
    public int lvl = 1, shotCnt;
    int lvlCnt = 15, rndLvlMin = 6, rndLvlMax = 15;
    int Lvl => GetLvl(Gc.Level, lvlCnt, rndLvlMin, rndLvlMax);
    int LvlIdx => Lvl - 1;
    [HideInInspector]
    public List<Enemy> enemies = new List<Enemy>();
    public HealthBar healthBarPf;
    public Enemy enemyPf;
    public GameObject expPf, dropPf;
    public void Init() {
        if (useLvl)
            Gc.Level = lvl;
        LoadLevel();
        LeaderBoardData.SetDatas();
    }
    public void LoadLevel() {
        int[] arr = Cc.I.ShopArr();
        WepTp wepTp = WepTp.Pistol;
        for (int i = 0; i < arr.Length; i++) {
            GameObject btnGo = Cc.I.ShopCol(i, arr[i]);
            if (btnGo.Tcc() > 1)
                btnGo.ChildGo(1).Txt().text = Cc.I.prices[i] + "$";
            if (arr[i] == 2)
                wepTp = (WepTp)i;
        }
        Level level = levels[LvlIdx];
        roomDatas[level.envTp - 1].Show();
        shotCnt = Player.I.shotCnt = level.shotCnt;
        level.enemies.ForEach(x => CrtEnemy(x));
        Player.I.Bullet();
        Player.I.Init(wepTp);
    }
    void CrtEnemy(Vector3 pos) {
        Enemy e = Ins(enemyPf, pos, Rnd.R, tf);
        enemies.Add(e);
        e.mGo = e.ChildGo(0, Rnd.Idx(11));
        e.mGo.Show();
    }
    int GetLvl(int lvl, int lvlCnt, int rnd1, int rnd2) {
        string lvlData = Data.LevelData.S();
        if (lvlData.IsNe()) {
            for (int i = 1; i <= lvlCnt; i++)
                lvlData += i + ",";
            Data.LevelData.Set(lvlData);
        }
        int[] arr = lvlData.SubLast(0, 1).IntArr();
        if (lvl <= arr.Length) {
            return arr[lvl - 1];
        } else {
            int prvLvl = arr.Last();
            for (int i = arr.Length + 1; i <= lvl; i++) {
                prvLvl = prvLvl == rnd1 ? Rnd.RngIn(rnd1 + 1, rnd2) : prvLvl == rnd2 ? Rnd.RngIn(rnd1, rnd2 - 1) : Rnd.P((prvLvl - rnd1).F() / (rnd2 - rnd1)) ? Rnd.RngIn(rnd1, prvLvl - 1) : Rnd.RngIn(prvLvl + 1, rnd2);
                lvlData += prvLvl + ",";
            }
            Data.LevelData.Set(lvlData);
            return prvLvl;
        }
    }
}
// ░ ▒ ▓ █ ▄ ▀ ■
// ┌ ┬ ┐ ─ ╔ ╦ ╗ ═
// ├ ┼ ┤ │ ╠ ╬ ╣ ║
// └ ┴ ┘   ╚ ╩ ╝
// ⁰¹²³⁴⁵⁶⁷⁸⁹ ⁻⁺⁼⁽⁾ ⁱⁿ superscript
// ₀₁₂₃₄₅₆₇₈₉ ₋₊₌₍₎⨧ ᵣᵤₐᵢⱼₓₑᵥₒₔ ᵪᵧᵦᵨᵩ subscript
// _ ¯ ~ ≡ ‗ ¦ ¨ ¬ · |
// ñ Ñ @ ¿ ? ¡ ! : / \ frequently-used
// á é í ó ú Á É Í Ó Ú vowels acute accent
// ä ë ï ö ü Ä Ë Ï Ö Ü vowels with diaresis
// ½ ¼ ¾ ¹ ³ ² ƒ ± × ÷ mathematical symbols
// $ £ ¥ ¢ ¤ ® © ª º ° commercial / trade symbols
// " ' ( ) [ ] { } « » quotes and parenthesis