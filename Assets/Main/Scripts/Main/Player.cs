﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public enum WepTp { Pistol, Pistol_Metal, Pistol_Small, Rifle, Shotgun, Sniper, MachineGun_Bandit, MachineGun_USA }

public class Player : Character {
    public static Player I {
        get {
            if (_.Null())
                _ = GameObject.FindObjectOfType<Player>();
            return _;
        }
    }
    static Player _;
    Vector3 camPosPistol = V3.Y(1.53f), camPosRifle = V3.V(-0.09f, 1.53f, -0.04f), camPosMachineGun = V3.V(-0.09f, 1.53f, -0.17f);
    Transform shotPntTf;
    public Vector2 mpSpd = V2.V(0.01f);
    public float spd = 2;
    public GameObject enterGo, exitGo;
    public Bullet bulletPf;
    public GameObject gunEffectPf;
    public WepTp wepTp;
    Vector3 rot, mp2, pos;
    [HideInInspector]
    public GameObject modelGo;
    public Text bulletTxt;
    bool isPistol, isMg, isRifle, isSearch = true;
    public int shotCnt;
    float rotX, t = 0, dy = 0;
    RaycastHit hit;
    private void Awake() {
        _ = this;
    }
    public void Reset() {
    }
    void Start() {
    }
    IEnumerator ShowHideCor(GameObject go, float tm = 1) {
        go.Show();
        yield return Wf.S(tm);
        go.Hide();
    }
    public bool IsEnemyEnter(Enemy e) {
        if (e.isLive && Physics.Raycast(modelGo.Tp(), e.mGo.Tp() - modelGo.Tp(), out hit, 10, Lm.Table | Lm.Enemy))
            if (hit.collider.Tag(Tag.Enemy))
                return true;
        return false;
    }
    List<Enemy> prvEnemies = new List<Enemy>(), nearEnemies = new List<Enemy>();
    void Update() {
        if (IsPlaying) {
            prvEnemies = nearEnemies.Copy();
            nearEnemies = new List<Enemy>();
            foreach (var e in Ls.I.enemies)
                if (IsEnemyEnter(e))
                    nearEnemies.Add(e);
            if (t > 1) {
                if (isSearch) {
                    if (nearEnemies.IsCount()) {
                        isSearch = false;
                        mp = Mp;
                        rb.V0();
                        t = 0;
                        StaCor(ShowHideCor(enterGo));
                    }
                } else {
                    if (nearEnemies.IsEmpty()) {
                        isSearch = true;
                        mp = Mp;
                        mp2 = Mp;
                        t = 0;
                        foreach (var e in Ls.I.enemies)
                            e.Reset();
                        StaCor(ShowHideCor(exitGo));
                    }
                }
            }
            if (isSearch) {
                if (IsMbD) {
                    mp = Mp;
                    mp2 = Mp;
                }
                if (IsMb) {
                    rot = rot.Y(rot.y + (Mp.x - mp.x) * mpSpd.x);
                    Te = rot.X(0);
                    modelGo.Tle(rot.Y(0));
                    rb.V(F * M.Remap(M.Abs(Mp.y - mp2.y), 0, Screen.width / 4, 0, spd));
                    mp = Mp;
                }
                if (IsMbU) {
                    rb.V0();
                }
                rot = rot.X(M.Move(rot.x, rotX, Dt * 45));
            } else {
                if (IsMbD) {
                    mp = Mp;
                }
                if (IsMb) {
                    rot = V3.Xy(M.C(rot.x + (mp.y - Mp.y) * mpSpd.y, -40, 90), rot.y + (Mp.x - mp.x) * mpSpd.x);
                    Te = rot.X(0);
                    modelGo.Tle(rot.Y(0));
                    mp = Mp;
                }
                if (IsMbU) {
                    if (TargetPos(ref pos) && Physics.SphereCast(Cm.I.Tp, 1, pos - Cm.I.Tp, out hit, 10, Lm.Enemy))
                        Shot();
                }
                RaycastHit[] hits = Physics.SphereCastAll(Cm.I.Tp, 0.2f, Cm.I.F, 10, Lm.Enemy);
                foreach (var hit2 in hits) {
                    Enemy e = hit2.collider.Par<Enemy>(1);
                    if (!e.isFear && nearEnemies.Contains(e))
                        e.Fear();
                }
            }
            t += Dt;
        } else {
            rb.V0();
        }
    }
    bool TargetPos(ref Vector3 pos) {
        Ray ray = Camera.main.ScreenPointToRay(V3.Xy(Screen.width / 2, Screen.height / 2));
        if (Physics.Raycast(ray, out hit)) {
            pos = hit.point;
            return true;
        }
        return false;
    }
    void Shot() {
        if (shotCnt > 0) {
            Bullet bullet = Ins(bulletPf, shotPntTf.position, Q.O, Ls.I.tf);
            bullet.pwr = 40;
            if (TargetPos(ref pos)) {
                bullet.rb.V((pos - bullet.Tp).normalized * 10);
            } else {
                bullet.rb.V(modelGo.transform.forward * 10);
            }
            Dst(Ins(gunEffectPf, Tp, Q.O, Gc.I.tf), 3);
            shotCnt--;
            Bullet();
        }
    }
    public void Bullet() {
        bulletTxt.text = shotCnt + "/" + Ls.I.shotCnt;
    }
    public void Init(WepTp wepTp) {
        WepTp prvWepTp = this.wepTp;
        this.wepTp = wepTp;
        isPistol = wepTp.tS().IsS("Pistol");
        isMg = wepTp.tS().IsS("Machine");
        isRifle = !isPistol && !isMg;
        modelGo = go.ChildGo(0);
        GameObject charGo = modelGo.ChildGo(0);
        rotX = Ang.Rep180(modelGo.Tle().x);
        rot.x = rotX;
        dy = -modelGo.Tlp().y;
        charGo.TlpY(dy);
        charGo.TleY(42);
        charGo.An().Play(isPistol ? "Pistol" : "Rifle");
        GameObject wepParGo = modelGo.ChildNameGo("Weapon"), wepGo = wepParGo.ChildNameGo(wepTp.tS());
        wepParGo.Tlp(0.09f, 1.4f + dy, 0.55f);
        wepParGo.Tle(-10, -10, 0);
        wepParGo.ChildNameHide(prvWepTp.tS());
        wepGo.Show();
        shotPntTf = wepGo.ChildName("ShotPnt");
        Cm.I.Tlp = (isPistol ? camPosPistol : isMg ? camPosMachineGun : camPosRifle) + V3.Y(dy);
    }
}