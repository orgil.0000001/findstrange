using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Orgil;

public class Enemy : Mb {
    int life = 100, fullLife = 100;
    public float lifeF => life.F() / fullLife;
    public bool isLive => life > 0;
    [HideInInspector]
    public bool isFear = false, isShake = false, isEscape = false;
    float t = 0;
    [HideInInspector]
    public GameObject cGo, mGo;
    HealthBar bar = null;
    private void Start() {
        cGo = go.ChildGo(0);
    }
    private void Update() {
        if (IsPlaying && isLive) {
            if (isFear) {
                t += Dt;
                if (!isShake && t > 1)
                    StaCor(ShakeCor(5, 0.2f, 10));
                if (!isEscape && t > 2)
                    StaCor(EscapeCor());
                if (!Player.I.IsEnemyEnter(this))
                    Reset();
            }
        }
    }
    public void Hit(int pwr) {
        if (isLive) {
            life -= pwr;
            Gc.Score += 10;
            Cc.I.HudScore(Gc.Score);
            if (bar) {
                if (!isLive) {
                    bar.AnimShow(false);
                    Dst(bar.go, 0.5f);
                }
            }
            if (!isLive) {
                StaCor(HideCor(0.5f));
                Gc.Kills++;
                Cc.I.HudLevelBar(Gc.Level, Gc.Kills.F() / Ls.I.enemies.Count);
                if (Gc.Kills == Ls.I.enemies.Count)
                    Gc.I.LevelCompleted();
            }
        }
    }
    IEnumerator HideCor(float tm) {
        for (float t = 0; t < tm; t += Dt) {
            cGo.Tls(V3.V(M.Lerp(1f, 0.01f, t / tm)));
            cGo.Tle(V3.Y(360f * t / tm));
            yield return null;
        }
        cGo.Hide();
    }
    public void Reset() {
        isFear = isShake = isEscape = false;
        if (bar)
            bar.AnimShow(false);
    }
    public void Fear() {
        isFear = true;
    }
    void HealthBar() {
        if (bar) {
            bar.AnimShow(true);
        } else {
            bar = Ins(Ls.I.healthBarPf, Tp, Cm.I.Tr, Ls.I.tf);
            bar.ChildGo(0).TlpY(mGo.Mr().bounds.size.y + 0.1f);
            bar.enemy = this;
        }
    }
    void SweatDrop() {
        GameObject go = Ins(Ls.I.dropPf, Tp, Q.O, tf);
        go.Child(0).Tlp(V3.Y(0.2f));
        go.Gc<Follow>().followTf = tf;
        Dst(go, 1);
    }
    IEnumerator ShakeCor(int n, float tm, float rot = 10) {
        SweatDrop();
        isShake = true;
        for (int i = 0; i < n; i++) {
            Vector3 a = i == 0 ? V3.O : i.IsEven() ? V3.Y(-rot) : V3.Y(rot), b = i == n - 1 ? V3.O : i.IsEven() ? V3.Y(rot) : V3.Y(-rot);
            for (float t = 0; t < tm; t += Dt) {
                cGo.Tle(V3.Lerp(a, b, t / tm));
                yield return null;
            }
        }
        if (IsPlaying && isLive)
            HealthBar();
    }
    IEnumerator EscapeCor() {
        isEscape = true;
        RaycastHit hit;
        if (Physics.Raycast(Tp + Ang.Xz(Rnd.Ang, Rnd.Rng(0.5f, 1f)).Y(3), V3.d, out hit, 10, Lm.Wall | Lm.Table)) {
            Vector3 v0 = Hpm.Ang_V0(Tp, hit.point, 45);
            if (v0.x.tS() != "NaN")
                rb.V(v0);
        } else {
            Vector3 v0 = Hpm.Ang_V0(Tp, Tp + Ang.Xz(Rnd.Ang, Rnd.Rng(0.5f, 1f)), 45);
            if (v0.x.tS() != "NaN")
                rb.V(v0);
        }
        yield return Wf.S(1);
        isEscape = false;
    }
}
